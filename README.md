# huni-app

This is the new repo for the HuNI web app.

It merges the following repos:

* https://bitbucket.org/huniteam/huni-app-interface
* https://bitbucket.org/huniteam/huni-backend

## Building images

The backend and frontend images are now built automatically by CircleCI.

  https://circleci.com/bb/huniteam/huni-app

The git revision is stored in the images. To check this, run:

  `docker inspect $IMAGE | grep GIT_SHA`

These are also available live at the following endpoints:

  * https://huni.net.au/version.json
  * https://huni.net.au/vlab/version.json

### Frontend image

The frontend image is `huniteam/huni-webapp-static:production`.

To trigger a build, create and push the `circleci-webapp-static` git tag.

### Backend image

The backend image is `huniteam/huni-webapp-api:production`.

To trigger a build, create and push the `circleci-webapp-api` git tag.
