#!/bin/bash -e

usage() {
    echo usage: $0 'frontend|backend|all' 'staging|production' 1>&2
    exit 1;
}

export HUNI_DEVELOPMENT=''

if [[ $# -ne 2 ]]; then
    usage
fi

if [[ $1 == 'frontend' || $1 == 'all' ]]; then
    docker-compose -p app build frontend
    docker tag app_frontend:latest huniteam/huni-webapp-static:$2
fi

if [[ $1 == 'backend' || $1 == 'all' ]]; then
    docker-compose -p app build backend
    docker tag app_backend:latest huniteam/huni-webapp-api:$2
fi
