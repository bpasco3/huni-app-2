#!/bin/bash

# This script gets called by clients to wait until the service is ready.
# The env.sh files are also printed to stdout so the output can be evalled.
# Only the env commands should go to stdout, everything else goes to stderr.

exec 3>&1   # Save stdout to fh 3
exec 1>&2   # Redirect all output to stderr by default

: ${SD_NEO4J_URI:=http://127.0.0.1:7474}

# Need to cope with:
#   http://user:password@host:port/
#   http://user:password@host:port
#   http://host:port/
#   http://host:port
# Split on punctuation into the PARTS array, and then index from the back.
PARTS=( $( echo $SD_NEO4J_URI | tr -s ':/@' ' ' ) )
SD_NEO4J_PORT=${PARTS[-1]}
SD_NEO4J_HOST=${PARTS[-2]}

# Test for port without nc
test-port() {
    host=$1
    port=$2

    ( dd bs=1 count=0 < /dev/tcp/$host/$port ) 2> /dev/null
}

wait-for-neo4j() {
    local msg="Waiting for neo4j to start up on $SD_NEO4J_URI"
    local count=1
    local max=30

    while [[ $count -le $max ]]; do
        echo $msg \(attempt $count of $max ...\)
        if test-port $SD_NEO4J_HOST $SD_NEO4J_PORT; then
            echo neo4j is ready
            return 0
        fi
        count=$(( $count + 1 ))
        sleep 2
    done
    echo Giving up
    return 1
}

wait-for-neo4j
cat /fixture/neo4j/env.sh        # cat to stderr for logging purposes
cat /fixture/neo4j/env.sh >&3    # cat to stdout for caller to eval
