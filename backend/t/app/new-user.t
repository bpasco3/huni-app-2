use HuNI::Backend::Test;

my $username = name('name');
my $agent = HuNI::Backend::Test::Agent->new(
    app => fixture->app,
    session => {
        'oauth2-user' => {
            id       => $username,
            name     => $username,
            provider => 'testing',
        },
    },
);

my $first = resp_is(200, $agent->get('/user'));
is($first->{new_user}, 1, 'This is a new user');

$agent = fixture->make_agent($first);
my $again = resp_is(200, $agent->get('/user'));
is($again->{new_user}, 0, 'This is a returning user');

for my $key (qw( display_name user_id )) {
    is($first->{$key}, $again->{$key}, "Matching $key");
}

done_testing();
