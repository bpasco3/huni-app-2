use HuNI::Backend::Test;

my $agent = fixture->make_agent(name('User'));
my $profile = $agent->build(profile())->{profile};

note 'Try fetching your own profile'; {
    my $ret = resp_is(200, $agent->get('/profile'));
    eq_or_diff($ret, $profile, 'Profiles match');
}

note 'Try fetching a profile anonymously'; {
    my $anon = fixture->make_agent;
    resp_is(403, $anon->get('/profile'));
}

ok(1);

done_testing;
