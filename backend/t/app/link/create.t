use HuNI::Backend::Test;

my $agent = fixture->make_agent(name('User'));
my $other = fixture->make_agent(name('User2'));

my $path = '/link';

note 'Create a valid link'; {
    my $linktype = $agent->build(linktype('Place', 'Person'))->{linktype};
    my $data = {
        linktype_id => $linktype->{id},
        from_docid  => name('docid'),
        to_docid    => name('docid'),
    };

    my $json = resp_is(200, $agent->post($path, $data));
    for my $key (qw( linktype_id from_docid to_docid )) {
        is($data->{$key}, $json->{$key}, "$key matches");
    }
    is($linktype->{pair_id}, $json->{pairtype_id}, 'pairtype_id matches');
    is($linktype->{name},    $json->{linktype},    'linktype matches');

    resp_is(400, $agent->post($path, $data), 'Can\'t create same link twice');
    resp_is(200, $other->post($path, $data), 'But someone else can');
}

note 'Create some bad links'; {
    my $linktype = $agent->build(linktype('Place', 'Event'))->{linktype};
    my $good_data = {
        linktype_id => $linktype->{id},
        from_docid  => name('docid'),
        to_docid    => name('docid'),
    };

    for my $missing (keys %$good_data) {
        my %data = %$good_data;
        delete $data{$missing};
        resp_is(404, $agent->post($path, \%data));
    }

    {
        my %data = %$good_data;
        $data{to_docid} = $data{from_docid};
        resp_is(400, $agent->post($path, \%data));
    }
}

done_testing();
