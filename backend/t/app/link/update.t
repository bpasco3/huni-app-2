use HuNI::Backend::Test;

my $agent = fixture->make_agent(name('User'));

my $path = '/link';

my $linktype = $agent->build(linktype('Place', 'Person'))->{linktype};
my $linkdata = link('place0', 'person0', $linktype);
my $link     = $agent->build($linkdata)->{link};


my $data = $linkdata->{link};
resp_is(200, $agent->put("$path/$link->{id}", $data));

# Check we can't modify a link to make from==to
resp_is(400, $agent->put("$path/$link->{id}", { to_docid => $data->{from_docid} }));

done_testing();
