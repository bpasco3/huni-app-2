use HuNI::Backend::Test;

my %arg = (
    'Collection One'    => [ ],
    'Collection Two'    => [qw( docA docB docC )],
    'Collection Three'  => [qw( docA docC docE docG )],
    'Collection Four'   => [qw( docE docG )],
    'Collection Five'   => [qw( docC )],
);

my $agent = fixture->make_agent(name('User'));
my %records;

for my $name (sort keys %arg) {
    my $docids = $arg{$name};

    # Add the records in reverse order, so they'll come back in the order listed
    my $spec = collection(name => $name, records => [ reverse @$docids ]);
    my $ret = $agent->build($spec)->{collection};
    $records{ $ret->{id} } = $docids;
}

note 'Check the collection records are returned correctly';
for my $id (sort keys %records) {
    my $json = resp_is(200, $agent->get("/collection/$id/record"));

    my $expected = $records{$id};
    my $got = [ map { $_->{docid} } @$json ];

    eq_or_diff($expected, $got, 'got correct docids in expected order');
}

note 'Check non-existent collections behave';
resp_is(404, $agent->get("/collection/12345/record"));
resp_is(404, $agent->get("/collection/badbadbad/record"));

note 'Check that you cannot access another users collection records'; {
    my $other = fixture->make_agent(name('User'));
    my ($id) = %records; # pick any id
    TODO: {
        local $TODO = 'Need to change exists-but-not-yours to 403';
        resp_is(403, $other->get("/collection/$id/record"));
    }
}

note 'Check that you cannot access collection records anonymously'; {
    my $anon = fixture->make_agent;
    my ($id) = %records; # pick any id
    resp_is(403, $anon->get("/collection/12345/record"));
    resp_is(403, $anon->get("/collection/badbadbad/record"));
    resp_is(403, $anon->get("/collection/$id/record"));
}

done_testing();
