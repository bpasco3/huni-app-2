use HuNI::Backend::Test;

# Note that this test just checks what you can and can't do given your role on
# a particular collection. It does no actual checking the the commands do what
# they ought.

use Function::Parameters qw( :strict );

run_tests();
done_testing;
exit;

#---

sub run_tests {

    my %users = map { $_ => fixture->make_user($_) } (qw( owner user third ));

    my $agent = fixture->make_agent($users{user});

    my %allowed = (
        fetch_collections   => [qw( owner read-write read-only      )],
        fetch_collection    => [qw( owner read-write read-only      )],
        list_records        => [qw( owner read-write read-only      )],
        add_record          => [qw( owner read-write                )],
        del_record          => [qw( owner read-write                )],
        del_collection      => [qw( owner                           )],
        invite_user         => [qw( owner                           )],
        del_user            => [qw( owner                           )],

        set_name            => [qw( owner                           )],
        set_is_public       => [qw( owner                           )],
        set_synopsis        => [qw( owner read-write                )],
        set_all             => [qw( owner                           )],
    );
    for my $op (keys %allowed) {
        my $roles = $allowed{$op};
        $allowed{$op} = { map { $_ => 1 } @$roles };
    }

    my @collections = create_collections(\%users);

    my @tests = (
        [ fetch_collection => \&test_fetch_collection                   ],
        [ list_records     => \&test_list_records                       ],
        [ add_record       => \&test_add_record                         ],
        [ del_record       => \&test_del_record                         ],
        [ invite_user      => \&test_invite_user                        ],
        [ del_user         => \&test_del_user                           ],
        [ set_name         => make_set_test({ name => 'The Name' })     ],
        [ set_is_public    => make_set_test({ is_public => 1 })         ],
        [ set_synopsis     => make_set_test({ synopsis => 'Synopsis' }) ],
        [ set_all          => make_set_test({
                                name => 'The Name',
                                is_public => 1,
                                synopsis => 'Synopsis', })              ],

        # This deletes collections, so must come last
        [ del_collection   => \&test_del_collection                     ],
    );

    note 'Testing fetch_collections'; {
        my $response = $agent->get('/collection');
        my $code = $response->code;
        ok(!$response->is_error, "No error for fetch_collections ($code)");
        my @got = sort map { $_->{name} } @{ $response->json };
        my @expected = sort keys %{ $allowed{fetch_collections} };
        eq_or_diff(\@got, \@expected, 'Fetched the expected collections');
    }

    for my $test (@tests) {
        my ($op, $run_test) = @$test;
        note "Testing $op";
        for my $collection (@collections) {
            my $role = $collection->name;

            my $response = $run_test->($collection, $agent, \%users);
            my $code = $response->code;
            ok(!$response->is_server_error,
                "No server error for $role $op ($code)");

            if ($allowed{$op}->{$role}) {
                ok($response->is_success, "Role $role can $op");
            }
            else {
                ok($response->is_error, "Role $role can NOT $op");
            }
        }
    }
}



#----

fun create_collections($users, $suffix = '') {
    my @collections;
    for my $role (qw( owner read-write read-only )) {
        my $name = $role;
        $name .= "-$suffix" if $suffix;

        my $collection = create_collection($name, $users->{owner});
        $collection->add_to_users(
            {
                id => $users->{user}->{user_id},
            },
            {
                inviting_user_id => $users->{owner}->{user_id},
                role_id => $role,
            });
        push(@collections, $collection);
    }
    push(@collections, create_collection('no-role', $users->{owner}));
    return @collections;
}

fun create_collection($name, $owner) {
    my $collection = fixture->resultset('Collection')->create({
        name => $name,
    });
    for (1 .. 1+int(rand(5))) {
        $collection->add_to_collection_records({
            user_id => $owner->{user_id},
            docid => "record-$_",
        });
    }
    return $collection;
}

#----

fun test_fetch_collection($collection, $agent, $users) {
    my $collection_id = $collection->id;
    my $response = $agent->get("/collection/$collection_id");
    return $response;
}

fun test_list_records($collection, $agent, $users) {
    my $collection_id = $collection->id;
    my $response = $agent->get("/collection/$collection_id/record");
    return $response;
}

fun test_add_record($collection, $agent, $users) {
    my $docid = 'add-record';
    my $collection_id = $collection->id;
    my $response = $agent->put("/collection/$collection_id/$docid");
    $collection->collection_records->search({ docid => $docid })->delete;
    return $response;
}

fun test_del_record($collection, $agent, $users) {
    my $docid = 'delete-record';
    my $collection_id = $collection->id;
    $collection->collection_records->create({
        docid => $docid, user_id => $users->{owner}->{user_id},
    });
    my $response = $agent->del("/collection/$collection_id/$docid");
    $collection->collection_records->search({ docid => $docid })->delete;
    return $response;
}

fun test_invite_user($collection, $agent, $users) {
    my $collection_id = $collection->id;
    my $user_id = $users->{third}->{user_id};
    my $response = $agent->put("/collection/$collection_id/user/$user_id/read-only");
    $collection->collection_users->search({ user_id => $user_id })->delete;
    return $response;
}

fun test_del_user($collection, $agent, $users) {
    my $collection_id = $collection->id;
    my $user_id = $users->{third}->{user_id};

    # Create a user that we can uninvite...
    $collection->add_to_users(
        { id => $user_id },
        { role_id => 'owner', inviting_user_id => $users->{owner}->{user_id} },
    );

    my $response = $agent->del("/collection/$collection_id/user/$user_id");
    $collection->collection_users->search({ user_id => $user_id })->delete;
    return $response;
}

# For obvious reasons, this one should come last...
fun test_del_collection($collection, $agent, $users) {
    my $collection_id = $collection->id;
    my $response = $agent->del("/collection/$collection_id");
    return $response;
}

fun make_set_test($data) {
    return fun($collection, $agent, $users) {
        my $collection_id = $collection->id;
        my $response = $agent->put("/collection/$collection_id", $data);
        return $response;
    };
}
