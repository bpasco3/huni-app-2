use HuNI::Backend::Test;


my $user = 'User000';
my $name = 'Collection One';
my $path = '/collection';
my @manual_keys = qw( name is_public synopsis user_id );
my @auto_keys   = qw( id created_utc modified_utc );
my @keys = ( @auto_keys, @manual_keys );
my $collection;

note 'Create a collection with unauthed user fails'; {
    my $agent = fixture->make_agent;
    resp_is(403, $agent->post($path, { name => $name }));
}

note 'Create a collection with insufficient parameters fails'; {
    my $agent = fixture->make_agent($user);

    resp_is(404, $agent->post($path));
    resp_is(404, $agent->post($path, { woo => 'hoo' }));
    resp_is(404, $agent->post($path, { name => '' }));
    resp_is(404, $agent->post($path, { name => ' ' }));
}

note 'Create a collection'; {
    my $agent = fixture->make_agent($user);

    my $args = {
        name => $name,
        synopsis => 'This is a collection',
    };

    my $before = timestamp_now();
    $collection = resp_is(200, $agent->post($path, $args));
    my $after = timestamp_now();

    is($collection->{name}, $args->{name}, 'name is correct');
    ok(! $collection->{is_public}, 'not public');
    is(@{ $collection->{records} }, 0, 'no records');
    is($collection->{synopsis}, $args->{synopsis}, 'synopsis matches');

    is($collection->{created_utc}, $collection->{modified_utc},
        'created & modified timestamps are the same');

    my $created = $collection->{created_utc};
    cmp_ok($before, 'lt', $created, 'created_utc is after request time');
    cmp_ok($created, 'lt', $after,  'created_utc is before response time');
}

note 'Read the new collection back and check that it matches'; {
    my $agent = fixture->make_agent($user);
    my $back = resp_is(200, $agent->get("$path/" . $collection->{id}));

    eq_or_diff($back, $collection);
}

note 'Create the same collection again, with nothing new'; {
    my $agent = fixture->make_agent($user);
    my $again = resp_is(200, $agent->post($path, {
        name => $collection->{name},
        synopsis => $collection->{synopsis},
    }));
    for my $key (@manual_keys) {
        is($collection->{$key}, $again->{$key}, "$key matches");
    }
    for my $key (@auto_keys) {
        isnt($collection->{$key}, $again->{$key}, "$key matches");
    }
}

note 'Garbage parameters are ignored'; {
    my $agent = fixture->make_agent($user);

    my %data = ( woo => 'hoo', foo => 'bar' );
    my $json = resp_is(200, $agent->post($path, { name => 'ok', %data }));

    for my $key (sort keys %data) {
        ok(! exists $json->{$key}, "No $key in return");
    }
}

done_testing();
