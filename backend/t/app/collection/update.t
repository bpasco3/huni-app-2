use HuNI::Backend::Test;

my $agent = fixture->make_agent(name('User'));

my %state = (
    name        => 'name0000',
    synopsis    => 'synopsis0000',
    is_public   => JSON::true,
);
my $collection = $agent->build(collection(%state))->{collection};

my $id = $collection->{id};
my $prev_modified_utc = $collection->{modified_utc};

my @updates = (
    [qw( name synopsis is_public )],
    [qw( synopsis is_public )],
    [qw( name is_public )],
    [qw( name synopsis )],
    [qw( synopsis )],
    [qw( is_public )],
    [qw( name )],
);

for my $update (@updates, @updates) {
    my $data = update_fields(@$update);

    my $json = resp_is(200, $agent->put("/collection/$id", $data));

    for my $key (sort keys %state) {
        is($json->{$key}, $state{$key}, "match: $key");
    }

    cmp_ok($json->{modified_utc}, 'gt', $prev_modified_utc,
        'modified timestamp got updated');
    $prev_modified_utc = $json->{modified_utc};
    is($json->{created_utc}, $collection->{created_utc},
        'creation timestamp unchanged');
}

note 'Try to update another users collection'; {
    my $other = fixture->make_agent(name('User'));
    TODO: {
        local $TODO = 'Need to change exists-but-not-yours to 403';
        resp_is(403, $other->put("/collection/$id", { name => 'Ha ha!' }));
    }
}

note 'Try to update a collection when anonymous'; {
    my $anon = fixture->make_agent;
    resp_is(403, $anon->put("/collection/$id", { name => 'Hurr hurr!' }));
}

note 'Try to update a non-existent collection';
resp_is(404, $agent->put("/collection/12345", { name => 'Ho ho!' }));

note 'Try to update an ill-formed collection';
resp_is(404, $agent->put("/collection/badbadbad", { name => 'Hee hee!' }));

note 'Try to rename a collection to an existing name'; {
    # This used to be disallowed, but is now fine.
    my $collectionA = $agent->build(collection())->{collection};
    my $collectionB = $agent->build(collection())->{collection};

    resp_is(200, $agent->put("/collection/$collectionA->{id}",
                                { name => $collectionB->{name} }));
}


done_testing();

fun update_fields(@fields) {
    my %update;
    for my $field (@fields) {
        if ($field eq 'is_public') {
            $state{$field} = json_not($state{$field});
        }
        else {
            $state{$field}++;
        }
        $update{$field} = $state{$field};
    }

    return \%update;
}

fun json_not($json_bool) {
    return ($json_bool == JSON::true) ? JSON::false : JSON::true;
}

