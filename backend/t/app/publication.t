use HuNI::Backend::Test;
use JSON;

my @data = qw(
    user_a;1;aaa;3
    user_a;1;bbb;4
    user_a;1;ccc;5
    user_a;0;ddd;6
    user_b;0;eee;1
    user_c;1;fff;4
    user_d;1;ggg;2
    user_d;1;hhh;7
);

my %agents;
my @collections;
my @publications;
for my $data (@data) {
    my ($username, $is_public, $collection_name, $count) = split(/;/, $data);

    my $agent = $agents{$username} //= fixture->make_agent($username);
    my $spec = collection(
        name => $collection_name,
        synopsis => "This is collection $collection_name",
        is_public => $is_public,
        record_count => $count,
    );

    my $collection = $agent->build($spec)->{collection};
    $collection->{owner_agent} = $agent;

    # Use unshift here as the collections here will be returned in MRU order
    unshift(@collections, $collection);
    unshift(@publications, $collection) if $is_public;
}

note 'Get all the collections'; {
    my $anon = fixture->make_agent;
    my $json = resp_is(200, $anon->get('/publication'));

    my @got = @{ $json->{publications} };
    is(@got, @publications, 'Got all the publications');
    is(@publications, $json->{total}, '... with a matching total field');

    eq_or_diff([ map { $_->{id} } @got ], [ map { $_->{id} } @publications ],
               '... and matching ids');
}

note 'Get all the collections as a logged-in user'; {
    # Choose a user that has public and private collections.
    # Make sure you don't get your own private collections.
    my $user = fixture->make_agent('user_a');
    my $json = resp_is(200, $user->get('/publication'));

    my @got = @{ $json->{publications} };
    is(@got, @publications, 'Got only the publications expected');
    is(@publications, $json->{total}, '... with a matching total field');

    eq_or_diff([ map { $_->{id} } @got ], [ map { $_->{id} } @publications ],
               '... and matching ids');
}


note 'Get one page of collections'; {
    my $anon = fixture->make_agent;
    my $json = resp_is(200, $anon->get('/publication?n=2'));

    my @got = @{ $json->{publications} };
    my @expected = @publications[0 .. 1];
    is(@got, @expected, 'Got the first page of publications');
    is(@publications, $json->{total}, '... with a correct total field');

    eq_or_diff([ map { $_->{id} } @got ], [ map { $_->{id} } @expected ],
               '... and matching ids');
}

note 'Get the third page of collections'; {
    my $anon = fixture->make_agent;
    my $json = resp_is(200, $anon->get('/publication?n=2&p=3'));

    my @got = @{ $json->{publications} };
    my @expected = @publications[4 .. 5];
    is(@got, @expected, 'Got the third page of publications');
    is(@publications, $json->{total}, '... with a correct total field');

    eq_or_diff([ map { $_->{id} } @got ], [ map { $_->{id} } @expected ],
               '... and matching ids');
}

note 'Get a large page of collections'; {
    my $anon = fixture->make_agent;
    my $json = resp_is(200, $anon->get('/publication?n=10'));

    my @got = @{ $json->{publications} };
    my @expected = @publications;
    is(@got, @expected, 'Got only the publications expected');
    is(@publications, $json->{total}, '... with a correct total field');

    eq_or_diff([ map { $_->{id} } @got ], [ map { $_->{id} } @expected ],
               '... and matching ids');
}

note 'Get a page of collections off the end'; {
    my $anon = fixture->make_agent;
    my $json = resp_is(200, $anon->get('/publication?n=2&p=4'));

    my @got = @{ $json->{publications} };
    my @expected = ();
    is(@got, @expected, 'Got no publications as expected');
    is(@publications, $json->{total}, '... but with a correct total field');
}

note 'Do a search on the collections'; {
    my $anon = fixture->make_agent;
    my $json = resp_is(200, $anon->get('/publication?q=a'));

    my @got = @{ $json->{publications} };
    my @expected = ( $publications[-1] );
    is(@got, @expected, 'Got one publication as expected');
    is(@expected, $json->{total}, '... with a correct total field');

    eq_or_diff([ map { $_->{id} } @got ], [ map { $_->{id} } @expected ],
               '... and matching ids');
}


note 'Check publication record counts'; {
    # Ask for counts for all the collections, plus some made up ones.
    my @ids = map { $_->{id} } @collections;
    push(@ids, 8675309, 'badid');

    my $query = join('&', map { "c=$_" } @ids);

    my $anon = fixture->make_agent;
    my $json = resp_is(200, $anon->get("/publication/count?$query"));

    my @expected_ids = sort
                       map { $_->{id} }
                       grep { $_->{is_public} }
                       @collections;

    eq_or_diff([ sort keys %$json ], \@expected_ids, "Got only public collections");

    my %got_counts = map { $_->{id} => $_->{record_count} } values %$json;
    my %expected_counts = map { $_->{id} => $_->{record_count} } @publications;

    eq_or_diff(\%got_counts, \%expected_counts, "With correct record counts");
}

note 'Check publication details'; {
    my $pub = $publications[0];

    my $anon = fixture->make_agent;
    my $json = resp_is(200, $anon->get("/publication/$pub->{id}"));

    is(scalar @{ $json->{owners} }, 1, "one owner");
    is(scalar @{ $json->{records} }, $pub->{record_count}, "Got record list");
    is(scalar $json->{record_count}, $pub->{record_count}, "And record count");

    my ($private) = grep { ! $_->{is_public} } @collections;
    resp_is(404, $anon->get("/publication/$private->{id}"), "Can't get private collection");
    my $owner = $private->{owner_agent};
    resp_is(200, $owner->get("/publication/$private->{id}"), "... unless you own it");
    resp_is(404, $anon->get("/publication/badid"));
}

done_testing();
