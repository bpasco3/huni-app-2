use HuNI::Backend::Test;


note '/isloggedin with logged in user'; {
    my $agent = fixture->make_agent(name('User'));
    my $json = resp_is(200, $agent->get('/user'));
    for my $key (qw( user_id display_name )) {
        ok(exists $json->{$key}, "Got $key");
    }
    eq_or_diff($json, $agent->{user}, 'User details match');
}

note '/isloggedin with no user'; {
    my $agent = fixture->make_agent;
    my $json = resp_is(200, $agent->get('/user'));
    eq_or_diff($json, {}, 'Got empty details with no user');
}

done_testing();
