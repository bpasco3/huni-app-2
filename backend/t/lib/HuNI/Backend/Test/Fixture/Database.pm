package HuNI::Backend::Test::Fixture::Database;

use 5.16.0;
use strict;

use Moo;

use DBI;

use App::Sqitch;
use App::Sqitch::Config;
use App::Sqitch::Target;
use Function::Parameters ':strict';
use HuNI::Backend::Schema;
use Test::PostgreSQL;
use Try::Tiny;

my $env = 'HUNI_TEST_PG';

has schema => (
    is => 'lazy',
    builder => method() {
        HuNI::Backend::Schema->connect($self->dsn);
    },
);

has dsn => (
    is => 'lazy',
    builder => 1,
);

has postgres => (
    is => 'lazy',
    builder => sub { Test::PostgreSQL->new() },
    predicate => 1,
);

method new_user(%args) {
    $self->complete_required_columns(User => \%args);

    return $self->schema->resultset('User')->create(\%args);
}

method new_collection($user, %args) {
    $self->complete_required_columns(Collection => \%args);
    # If we're creating a collection, then the user is the owner, which means
    # they've invited themselves, and they're not pending.
    my $role = {
        inviting_user   => $user,
        role_id         => 'owner',
    };
    return $user->add_to_collections(\%args, $role);
}

method new_record($collection, $user, %args) {
    $args{user} //= $user;
    $self->complete_required_columns(CollectionRecord => \%args);

    return $collection->add_to_collection_records(\%args);
}

method complete_required_columns($rsname, $args) {
    $args->{$_} //= _uniq($_) for $self->required_columns($rsname);
}

method required_columns($rsname) {
    my $resultset = $self->schema->resultset($rsname);

    my $is_required = fun($info) {
        !$info->{is_nullable} &&
        !exists $info->{default_value} &&
        !exists $info->{is_auto_increment} &&
        !exists $info->{is_foreign_key}
    };

    my $column = $resultset->result_source->columns_info;
    my @required = grep { $is_required->($column->{$_}) } sort keys %$column;
    return @required;
}

fun _uniq($prefix) {
    state $next = { };
    return $prefix . $next->{$prefix}++;
}

method _build_dsn() {
    # If there is a postgres running, use it, otherwise spin up a new one.
    my $dsn = $ENV{$env} // $self->postgres->dsn;

    # Create a new database instance.
    $dsn = $self->_create_database($dsn);

    # And deploy our schema to it, if we aren't cloning template databases.
    if (! exists $ENV{HUNI_TEST_TEMPLATE_DB}) {
        $self->_deploy($dsn);
    }

    return $dsn;
}

fun _dsn_to_uri($dsn) {
    my $args = ($dsn =~ s/^DBI:Pg://ri);
    my %args = split(/;|=/, $args);
    $args{port} //= 5432;
    $args{user} .= ":$args{password}" if $args{password};
    return "db:pg://$args{user}\@$args{host}:$args{port}/$args{dbname}";
}

method _deploy($dsn) {
    my $uri = _dsn_to_uri($dsn);

    my $engine = try {
        my $config = App::Sqitch::Config->new;
        my $sqitch = App::Sqitch->new(verbosity => 0, config => $config);
        my $target = App::Sqitch::Target->new(
            sqitch => $sqitch,
            uri => URI::db->new($uri),
        );
        $target->engine->deploy;
    }
    catch {
        die "sqitch deployment failed: $_";
    };
}

method _create_database($dsn) {

    # Create a unique db name so we can run tests in parallel
    my $unique = $$ . time;
    my $prefix = $ENV{HUNI_TEST_DB_PREFIX} // 'huni_test_';
    my $dbname = $prefix . $unique;
    my $dbh = DBI->connect($dsn, '', '', {}) or die $DBI::errstr;

    my $template = '';
    if (exists $ENV{HUNI_TEST_TEMPLATE_DB}) {
        $template = " WITH TEMPLATE '$ENV{HUNI_TEST_TEMPLATE_DB}'";
    }

    my @sql = (
        "SET client_min_messages TO 'WARNING'",
        "DROP DATABASE IF EXISTS $dbname",
        "CREATE DATABASE $dbname $template",
        "RESET client_min_messages",
    );
    for (@sql) {
        $dbh->do($_) or die $dbh->errstr;
    }

    $dsn =~ s/dbname=.*?;/dbname=$dbname;/;
    return $dsn;
}

1;
