use HuNI::Backend::Test;

my $linktypes = fixture->resultset('Linktype');
my $links = fixture->resultset('Link');

my $linktype;
lives_ok {
    $linktype = $linktypes->find_or_create_linktype(
        from => 'Person',
        to   => 'Work',
        name => 'Inventor Of',
        pair => 'Invented By',
        user_id => 1,
    );
} 'Create a linktype to work with';

#use Data::Dumper::Concise; print STDERR Dumper($linktype);

my %linkdata = (
    user_id    => 1,
    from_docid => 'from0',
    to_docid   => 'to0',
    linktype_id => $linktype->id,
);

lives_ok {
    $links->create(\%linkdata);
} 'Create a link';

throws_ok {
    $links->create(\%linkdata);
} qr/duplicate/i, 'Create the same link fails';

throws_ok {
    $links->create({
        user_id    => 1,
        from_docid => $linkdata{to_docid},
        to_docid   => $linkdata{from_docid},
        linktype_id => $linktype->pair_id,
    });
} qr/Duplicate link exists/, 'Create the reverse link fails';

done_testing();
