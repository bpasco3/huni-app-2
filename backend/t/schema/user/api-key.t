use HuNI::Backend::Test;

my $api_hash_method = 'pbkdf2'; # Update this as necessary
my $users = fixture->resultset('User');

my $user = $users->create({
    name => 'User 000', auth_userid => '000', auth_domain => 'test'
});
my $user2 = $users->create({
    name => 'User 001', auth_userid => '001', auth_domain => 'test'
});

my ($api_key, $api_username);

subtest "Generate API key from scratch" => sub {
    lives_ok { $api_key = $user->generate_api_key } 'Generate API key okay';

    is_not_blank($api_key, 'API key');

    $api_username = $user->api_username;
    is_not_blank($api_username, 'API username');

    is_not_blank($user->api_hashed_key, 'Hashed API key');
    is($user->api_hash_method, $api_hash_method, 'Hash method as expected');

    ok($user->check_api_key($api_key), 'Correct API key succeeds');

    for my $incorrect ($user->api_hashed_key, $user->api_username, '') {
        ok(! $user->check_api_key($incorrect),
            "Incorrect API key \"$incorrect\" fails");
    }
};

subtest "Generate a new API key" => sub {
    my $old_api_key = $api_key;

    lives_ok { $api_key = $user->generate_api_key }
        'Generate another API key okay';

    isnt($api_key, $old_api_key, 'New API key is different');
    is($user->api_username, $api_username, 'API username hasn\'t changed');

    ok($user->check_api_key($api_key), 'New API key succeeds');
    ok(! $user->check_api_key($old_api_key), 'Old API key fails');
};

subtest "Automatic hash method upgrade" => sub {
    $api_key = 'whoops';
    my $plain_method = 'plaintext';
    lives_ok {
        $user->update({
            api_hashed_key => $api_key,
            api_hash_method => $plain_method,
        })
    } 'Force the user to have a plaintext api key';

    is($user->api_hashed_key, $api_key,
        'User has plain-hashed key as expected');
    is($user->api_hash_method, $plain_method, "Hash method is $plain_method");

    # Unsuccessfully check this api key, which should do nothing.
    ok(! $user->check_api_key("WRONG"), 'Wrong password fails');
    is($user->api_hash_method, $plain_method,
        "Hash method is still $plain_method");

    # Successfully check this api key, which should trigger an upgrade.
    ok($user->check_api_key($api_key), 'Right password succeeds');

    is($user->api_hash_method, $api_hash_method,
            'Hash method automatically upgraded');

    ok($user->check_api_key($api_key), 'Password still succeeds');
};

subtest "Find user from api credentials" => sub {
    my $found = $users->find_from_api($api_username, $api_key);

    ok($found, 'Found the user from the api credentials');
    is($found->{user_id}, $user->id, 'User is the one we expect');

    ok(! $users->find_from_api($api_username, 'bad-key'),
        'No user with bad key');
    ok(! $users->find_from_api('bad-username', $api_key),
        'No user with bad username');
    ok(! $users->find_from_api('', ''), 'No user with empty credentials');
};

done_testing();

fun is_not_blank($x, $name) {
    local $Test::Builder::Level = $Test::Builder::Level + 1;

    ok($x =~ /\S/, "$name is not blank");
}
