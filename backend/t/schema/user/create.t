use HuNI::Backend::Test;

my $users = fixture->resultset('User');

lives_ok {
    $users->create({
        name => 'User 000', auth_userid => '000', auth_domain => 'test'
    })
} 'created a new user';

throws_ok {
    $users->create({
        name => 'User 000', auth_userid => '000', auth_domain => 'test'
    })
} qr/unique constraint/, 'cannot create a new user with same auth id/domain';

my %required = (
    name            => 'User 001',
    auth_userid     => '001',
    auth_domain     => 'test',
);
for my $key (sort keys %required) {
    my %args = %required;
    delete $args{$key};
    throws_ok {
        $users->create(\%args);
    } qr/not-null constraint/, "column is required: $key";
}

done_testing();
