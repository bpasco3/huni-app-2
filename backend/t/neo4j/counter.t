use HuNI::Backend::Test;
use HuNI::Backend::WebApp::Util;
use REST::Neo4j::Util qw( update_counter with_counter );

use 5.16.0;

use Function::Parameters qw( :strict );

my $neo = fixture->neo;
my $type = 'Test';
my $counter = 'testing';


my $callbacks = 0;
with_counter($neo, $counter, fun($transaction_id, $old_value) {
    $callbacks++;

    my $new_value = $old_value + 1;

    if ($callbacks == 1) {
        # Deliberately update the counter. This should cause the with_counter
        # to fail and retry.
        update_counter($neo, 'commit', $counter, $old_value, $new_value);
    }

    return $new_value;
});

is($callbacks, 2, "First callback failed, second succeeded");

done_testing;

#------------------------------------------------------------------------------

