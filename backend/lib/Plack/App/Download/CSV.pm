package Plack::App::Download::CSV;
use parent qw( Plack::Component );

use 5.16.0;
use warnings;

use Plack::Request ();

use Encode qw( encode );
use Function::Parameters qw( :strict );
use JSON qw( decode_json );
use Text::CSV ();

method call($env) {
    my $req = Plack::Request->new($env);

    my $data = eval { get_csv($req->parameters->{data}) }
        or return $req->new_response(400)->finalize;

    my $headers = build_headers(%$data);

    return $req->new_response(200, $headers, $data->{csv})->finalize;
}

fun get_csv($content) {
    my $json = decode_json($content);

    my $filename = $json->{filename} or die;
    my $columns  = $json->{columns}  or die;
    my $rows     = $json->{rows}     or die;

    my $csv = Text::CSV->new({ binary => 1 });
    my @data;
    for my $row ($columns, @$rows) {
        $csv->combine(@$row) or die $csv->error_input;
        push(@data, $csv->string);
    }

    return {
        filename => $filename, # encode this too?
        csv      => encode('utf8', join("\n", @data)),
    };
}

fun build_headers(:$filename, :$csv) {
    return [
        'Content-Disposition'       => "attachment; filename=$filename",
        'Content-type'              => 'text/csv',
        'Content-Length'            => length $csv,
        'Content-Transfer-Encoding' => 'binary',

        'Cache-Control'             => 'private',
        'Pragma'                    => 'private',
        'Expires'                   => 'Mon, 26 Jul 1997 05:00:00 GMT',
    ];
}

1;

__END__

=head1 NAME

Plack::App::Download::CSV;

=cut
