package App::Sqitch::Command::dsn;

use 5.16.0;
use strict;
use warnings;
use utf8;
use Moo;
use Locale::TextDomain 1.20 qw(App-Sqitch);
use App::Sqitch::Types qw(Str Target);
extends 'App::Sqitch::Command';

has target => (
    is  => 'ro',
    isa => Str,
);

sub options {
    return qw(
        target|t=s
    );
}

sub execute {
    my ( $self, $target ) = @_;

    # Need to set up the target before we do anything else.
    if (my $t = $self->target // $target) {
        $self->warn(__x(
            'Both the --target option and the target argument passed; using {option}',
            option => $self->target
        )) if $target && $self->target;
        require App::Sqitch::Target;
        $target = App::Sqitch::Target->new(sqitch => $self->sqitch, name => $t);
    } else {
        $target = $self->default_target;
    }
    my $engine = $target->engine;

    my $uri = $engine->uri;
    my $dsn = $uri->dbi_dsn;

    my @extra = qw( user password );
    for my $method (@extra) {
        my $value = $uri->$method or next;
        $dsn .= ";$method=$value";
    }
    say $dsn;
}

1;
