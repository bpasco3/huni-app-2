package REST::Neo4j::Util;

use 5.22.0;
use warnings;

use Carp                    qw( croak );
use DateTime                qw( );
use Exporter                qw( import );
use Function::Parameters    qw( :strict );
use Log::Any                qw( $log );
use Try::Tiny;

our @EXPORT_OK = qw(
    as_hashrefs index_by normalise_links swap_values
    inflate_timestamps
    get_counter update_counter with_counter
);

fun as_hashrefs($results) {
    my @columns = @{ $results->{columns} };

    my @out;
    for my $row_in (@{ $results->{data} }) {
        my %row_out;
        @row_out{@columns} = @{ $row_in->{row} };
        push(@out, \%row_out);
    }

    return \@out;
}

fun index_by($array, $key) {
    return { map { $_->{$key} => $_ } @$array };
}

fun swap_values($hash, $key1, $key2) {
    ($hash->{$key1}, $hash->{$key2}) = ($hash->{$key2}, $hash->{$key1});
}

fun get_counter($neo4j, $transaction_id, $counter_id) {
    my @args = (
        transaction_id => $transaction_id,
        parameters => { counter_id => $counter_id },
    );

    my $result = $neo4j->do(<<'...', @args);
MERGE (c:Counter { id:{ counter_id } })
    ON CREATE SET c.value = 1
RETURN c.value
...

    return $result->{data}->[0]->{row}->[0];
}

# Update the counter in such a way that conflicts are detected. We use a similar
# atomic compare-and-set approach to lock-free programming.
#
# The logic is effectively:
#   die if (counter.value != old_value);
#   counter.value = new_value;
#
# The way we do this is with a MERGE statement. If the counter still has the
# old value, it will be matched by the merge, and updated. If the counter has
# been modified along the way, the merge will attempt to create a new counter,
# but this will violate the uniqueness constraint, and fail.
fun update_counter($neo4j, $transaction_id, $counter_id, $old_value, $new_value) {
    return if $old_value == $new_value;

    my @args = (
        transaction_id => $transaction_id,
        parameters => {
            counter_id => $counter_id,
            old_value  => $old_value,
            new_value  => $new_value,
        },
    );

    my $result = $neo4j->do(<<'...', @args);
MERGE (c:Counter {id:{ counter_id }, value:{ old_value }})
    ON MATCH SET c.value = { new_value }
...
}

fun with_counter($neo4j, $counter_id, $f) {
    my $done = 0;
    my $attempt = 0;
    my $msg = "with_counter(\"$counter_id\"):";

    my $ret = 1;
    while (!$done) {
        my $transaction_id = $neo4j->begin;
        my $old_value = get_counter($neo4j, $transaction_id, $counter_id);
        $attempt++;

        my $new_value = try {
            my $new_value = $f->($transaction_id, $old_value // 0);

            if (defined $new_value) {
                # The user returned a new value (which may or may not be the
                # same as the old one. Update it if necessary and commit the
                # transaction.
                update_counter($neo4j, $transaction_id,
                                    $counter_id, $old_value, $new_value);
                $neo4j->commit($transaction_id);

                $log->info("$msg succeeded after $attempt attempts")
                    if $attempt > 1;
            }
            else {
                # The user returned undefined, which indicates the transaction
                # should be aborted.
                $log->info("$msg aborted by user");
                $neo4j->rollback($transaction_id);
                $ret = 0;
            }
            $done = 1;
        }
        catch {
            my $conflict = 'Neo.ClientError.Schema.ConstraintValidationFailed';
            if (neo4j_error($_) eq $conflict) {
                $log->info("$msg retry #$attempt");
            }
            else {
                # Roll back the transaction and rethrow the error.
                $neo4j->rollback($transaction_id);
                $log->error($msg . ' ' . neo4j_message($_));
                die $_;
            }
        };
    }
    return $ret;
}

fun neo4j_error($x) {
    return (ref $x eq 'HASH') && (exists $x->{code}) ? $x->{code} : '';
}

fun neo4j_message($x) {
    return (ref $x eq 'HASH') && (exists $x->{message}) ? $x->{message} : $x;
}

fun normalise_links($links, $docid) {
    my @fields_to_swap = (
        [qw( from_docid     to_docid    )],
        [qw( linktype_id    pairtype_id )],
        [qw( linktype       pairtype    )],
    );
    for my $link (@$links) {
        inflate_timestamps($link);
        next unless $link->{to_docid} eq $docid;
        swap_values($link, @$_) for @fields_to_swap;
    }
}

fun inflate_timestamps($obj) {
    for my $key (qw{ created modified }) {
        next unless exists $obj->{$key};

        my $millis = delete $obj->{$key};
        $obj->{$key . '_utc'} = DateTime->from_epoch(epoch => $millis / 1000)
                                        ->strftime('%FT%T.%3N Z');
    }
    return $obj;
}

fun inflate_timestamp($neo_timestamp) {
    DateTime->from_epoch(epoch => $neo_timestamp / 1000)
            ->strftime('%FT%T.%3N Z');
}

1;
