package REST::Neo4j::Schema;

use 5.22.0;
use warnings;

use Carp                    qw( croak );
use DateTime                qw( );
use Function::Parameters    qw( :strict );
use Log::Any                qw( $log );
use REST::Neo4j             qw( );
use Try::Tiny;
use YAML::XS                qw( LoadFile );

use Moo;

has neo => (
    is => 'ro',
    required => 1,
);

has config => (
    # This can be either a config hash, or a yaml filename
    is => 'ro',
    required => 1,
    coerce => fun($config) { ref $config ? $config : LoadFile($config) },
);

has label => (
    is => 'lazy',
    builder => method() {
        $self->config->{label} // 'SchemaDeployment'
    },
);

method deploy() {
    my $from = $self->deployed_version;
    my $to   = $self->schema_version;
    $log->info("Deployed version: $from");
    $log->info("Schema version:   $to");

    my $ok = 1;
    try {
        $self->deploy_range($from, $to);
    }
    catch {
        print $_;
        $ok = 0;
        $self->revert_range($self->deployed_version, $from);
    };

    return $ok;
}

method deploy_range($from, $to) {
    return if $from >= $to;

    $log->info("Deploying to version $to");
    for (my $version = $from + 1; $version <= $to; $version++) {
        $self->deploy_version($version);
    }
}

method revert_range($from, $to) {
    return if $from <= $to;

    $log->info("Reverting to version $to");
    for (my $version = $from; $version > $to; $version--) {
        $self->revert_version($version);
    }
}

method schema_version() {
    return scalar @{ $self->config->{versions} };
}

method deployed_version() {
    my $label = $self->label;
    my $result = $self->neo->do(
        "MATCH (d:$label) RETURN d.version ORDER BY d.sequence DESC LIMIT 1");

    my @data = @{ $result->{data} }
        or return 0;

    return $data[0]->{row}->[0];
}

method deployment_history() {
    my $label = $self->label;
    my $result = $self->neo->do(
        "MATCH (d:$label) RETURN d ORDER BY d.seq ASC");

    my @history = map { $_->{row}->[0] } @{ $result->{data} };
    for (@history) {
        $_->{timestamp} = DateTime->from_epoch(epoch => $_->{timestamp} / 1000)
    }
    return @history;
}

method deploy_version($version) {
    $log->info('Deploying version ' . $version);
    $self->execute_step(deploy => $version);
}

method revert_version($version) {
    $log->info('Reverting version ' . $version);
    $self->execute_step(revert => $version);
}

method execute_step($type, $version) {
    my $index = $version - 1;
    if (($index < 0) || ($index >= $self->schema_version)) {
        croak "Version $version out of range";
    }

    my $spec = $self->config->{versions}->[$index];

    my $statements = $spec->{$type}
        or croak "Version $version missing $type section";

    if ((ref $statements ne 'ARRAY') || (@$statements == 0)) {
        croak "Version $version $type section must be a non-empty array";
    }

    $statements = [ map { expand_env($_) } @$statements ];

    my $transaction_id = $self->neo->begin;
    for my $statement (@$statements) {
        $self->neo->do($statement, transaction_id => $transaction_id);
    }
    $self->neo->commit($transaction_id);

    my $new_version = $type eq 'deploy' ? $version : $version - 1;

    my $label = $self->label;
    $self->neo->do("MATCH (ds:$label) WITH count(ds) as seq CREATE (d:$label { type:{ type }, version:{ version }, timestamp:timestamp(), sequence:seq, statements:{ statements } }) RETURN d", parameters => { type => $type, version => $new_version, statements => $statements });
}

fun expand_env($text) {
    $log->info("Before: $text");
    $text =~ s{\$([A-Z_](?:[A-Z0-9_]*))}
              {$ENV{$1} // die "$text: \$$1 not found"}eg;
    $log->info("After : $text");
    return $text;
}

1;
