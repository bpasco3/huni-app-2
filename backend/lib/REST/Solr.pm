package REST::Solr;

use 5.22.0;
use warnings;

use Function::Parameters    qw( :strict );
use URI::Escape             qw( uri_escape );

use Moo;
extends qw( REST::Base );

# Returns doc|undef. Dies on multiple matches.
method find_docid($docid) {
    my @found = $self->_query($docid);

    return unless @found; # quietly return nothing if none found
    return $found[0] if @found == 1;

    my @matches = sort map { $_->{docid} } @found;
    my $matches = @matches;
    local $, = ', ';
    die "$docid matched $matches results: " . join(', ', @matches) . "\n";
}

# Returns a hash of docid => doc|undef.
method find_docids(@docids) {
    my @found = $self->_query(@docids);
    my %found = map { $_->{docid} => $_ } @found;
    return { map { $_ => $found{$_} } @docids };
}

method _query(@docids) {
    # Using json body parameters in a post request allows many more docids
    # than do query parameters in a get.
    my $data = {
        params => {
            wt => 'json',
            q => join(' OR ', map { 'docid:' . escape($_) } @docids),
            rows => scalar @docids,
        },
    };
    my $result = $self->post('', data => $data);
    my @docs = @{ $result->{response}->{docs} };

    for my $doc (@docs) {
        add_display_name($doc);
        add_search_text($doc);
    }

    return @docs;
}

fun escape($docid) {
    return $docid =~ s/\*/\\*/gr;
}

my @skip_fields = qw(
    displayName docid document_history entityType mentions primaryName
    primaryTitle provider_source searchText _version_
);
my %is_skip_field = map { $_ => 1 } @skip_fields;

fun add_search_text($doc) {
    # Create a new search text here, even if one already exists.
    my $search_text = lc join(' ',
        map  { $doc->{$_} }
        grep { $_ !~ /^source/ }    # skip fields starting with source
        grep { ! $is_skip_field{$_} }
             keys %$doc);

    $doc->{searchText} = $search_text;
}

my @display_name_fields = qw(
    name individualName familyName
);
fun add_display_name($doc) {
    # Create a new display name here, even if one already exists.
    my @names = grep { defined $_ }
                map { $doc->{$_} }
                    @display_name_fields;

    $doc->{displayName} = @names ? join(' ', @names) : '[untitled]';
}

method record_count() {
    my $data = {
        params => {
            wt => 'json',
            q  => 'docid:*',
            rows => 0,
        },
    };
    my $result = $self->post('', data => $data);
    return $result->{response}->{numFound};
}

1;
