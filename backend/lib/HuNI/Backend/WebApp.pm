package HuNI::Backend::WebApp;
use Dancer2;
our $VERSION = '0.1';

#------------------------------------------------------------------------------
package HuNI::Backend::WebApp::Public;
use Dancer2;

get '/version.json' => sub {
    return { git => $ENV{GIT_SHA} // 'development' };
};

any '/logout'  => sub { app->destroy_session };

use HuNI::Backend::WebApp::Handler::Publication;

get '/publication'        => \&fetch_publications;
get '/publication/count'  => \&fetch_publication_counts;

use HuNI::Backend::WebApp::Handler::LinkType;

get '/linktype/:from/:to' => \&fetch_linktypes;

use HuNI::Backend::WebApp::Handler::HealthCheck;

get '/healthcheck' => \&health_check;
get '/status'      => \&health_check;

use HuNI::Backend::WebApp::Handler::CMS;

get '/cms'               => \&cms_list;
get '/cms/history/:id'   => \&cms_history;
get '/cms/:id/:version?' => \&cms_get;
get '/cms/:id'           => \&cms_get;

use HuNI::Backend::WebApp::Handler::Profile;

get '/profile/find' => \&find_profiles;

#------------------------------------------------------------------------------
package HuNI::Backend::WebApp::PublicMaybeLoggedIn;
use Dancer2;
use HuNI::Backend::WebApp::User qw( may_be_logged_in );

hook before => \&may_be_logged_in;

use HuNI::Backend::WebApp::Handler::Login;

get '/user'    => \&is_logged_in;

use HuNI::Backend::WebApp::Handler::Record;

get '/record/collection'     => \&fetch_collections_for_records;
get '/record/link'           => \&fetch_links_for_records;
get '/record/:id/collection' => \&fetch_record_collections;
get '/record/:id/link'       => \&fetch_record_links;

use HuNI::Backend::WebApp::Handler::Link;

get '/link/:id'              => \&fetch_link_details;

use HuNI::Backend::WebApp::Handler::Profile;

get '/profile/:id' => \&fetch_profile_for_user;

use HuNI::Backend::WebApp::Handler::Publication;

get '/publication/:id'       => \&fetch_publication_details;

#------------------------------------------------------------------------------
package HuNI::Backend::WebApp::Private;
use Dancer2;
use HuNI::Backend::WebApp::User qw( must_be_logged_in );

hook before => \&must_be_logged_in;

use HuNI::Backend::WebApp::Handler::Profile;

get  '/profile' => \&fetch_profile;
put  '/profile' => \&update_profile;
post '/apikey'  => \&generate_api_key;

use HuNI::Backend::WebApp::Handler::Collection;

get  '/collection'            => \&fetch_collections;
post '/collection'            => \&fetch_or_create_collection;
get  '/collection/:id'        => \&fetch_collection_details;
get  '/collection/:id/record' => \&fetch_collection_records;
put  '/collection/:id'        => \&set_collection_metadata;
del  '/collection/:id'        => \&delete_collection;

get  '/collection/:id/user/suggest'           => \&get_user_suggestions;
put  '/collection/:id/user/:user_id/:role_id' => \&add_user_to_collection;
del  '/collection/:id/user/:user_id'          => \&delete_user_from_collection;
del  '/collection/:id/user'                   => \&delete_self_from_collection;
get  '/collection/:id/user'                   => \&get_users_for_collection;

# These need to come after '/collection/:id/user' as they clash.
put  '/collection/:id/:docid' => \&add_collection_document;
del  '/collection/:id/:docid' => \&delete_collection_document;

use HuNI::Backend::WebApp::Handler::Link;

get  '/link'        => \&fetch_links;
post '/link'        => \&create_link;
post '/link/batch'  => \&add_link_batch;
put  '/link/:id'    => \&update_link;
del  '/link/:id'    => \&delete_link;
get '/link/:from_docid/:to_docid/:linktype_id'  => \&find_link;

use HuNI::Backend::WebApp::Handler::LinkType;

post '/linktype' => \&create_linktype;

use HuNI::Backend::WebApp::Handler::CMS;

put '/cms/:id'  => \&cms_put;

1;
