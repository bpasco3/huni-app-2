use utf8;
package HuNI::Backend::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces(
    default_resultset_class => "+HuNI::Backend::Schema::ResultSet",
);


# Created by DBIx::Class::Schema::Loader v0.07037 @ 2014-04-01 21:19:25
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:iKPsH/iM/5CxNfbq4fRajA

use Function::Parameters qw( :strict );

# Here is where we add connection parameters for the database connection.
# 1. there is only one place to specify them
# 2. using FP:strict means callers can't inadvertently specify them elsewhere
method connection($class: $dsn) {
    $class->SUPER::connection($dsn, '', '', {
        quote_names => 1,
    });
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
