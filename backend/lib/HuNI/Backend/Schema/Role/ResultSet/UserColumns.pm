package HuNI::Backend::Schema::Role::ResultSet::UserColumns;

use Role::Tiny;
use Function::Parameters qw( :strict );

method with_is_mine($user_id) {
    my $me = $self->current_source_alias;
    my $dbh = $self->result_source->storage->dbh;
    my $column = $dbh->quote_identifier(undef, $me, 'user_id');
    return $self->search(undef, {
        '+select' => [ \qq($column = $user_id) ],
        '+as'     => [ 'is_mine' ],
    });
}

method with_username() {
    return $self->search(undef, {
        '+select' => [qw( user.name )],
        '+as'     => [qw( username  )],
        join      => [qw( user      )],
    });
}

1;

__END__

=head1 NAME

HuNI::Backend::Schema::Role::ResultSet::UserColumns

=head1 SYNOPSIS

In your ResultSet class:

    use Role::Tiny::With;
    with 'HuNI::Backend::Schema::Role::ResultSet::UserColumns';

=head1 DESCRIPTION

Helper methods for ResultSet classes that have a user_id column.

=head1 METHODS

=head2 with_is_mine ($user_id)

Add a synthetic boolean column 'is_mine' which is true if the user_id column
matches $user_id

=head2 with_username

Add a synthetic column 'username' containing user.name

=cut
