package HuNI::Backend::Schema::Role::ResultSet::Timestamps;

use Role::Tiny;
use Function::Parameters qw( :strict );

method sort_by_created ($direction = 'desc') {
    my $me = $self->current_source_alias;
    return $self->search({ },
        { order_by => { '-' . $direction => "$me.created_utc" }});
}

method sort_by_modified ($direction = 'desc') {
    my $me = $self->current_source_alias;
    return $self->search({ },
        { order_by => { '-' . $direction => "$me.modified_utc" }});
}

1;

__END__

=head1 NAME

HuNI::Backend::Schema::Role::ResultSet::Timestamps

=head1 SYNOPSIS

In your ResultSet class:

    use Role::Tiny::With;
    with 'HuNI::Backend::Schema::Role::ResultSet::Timestamps';

=head1 DESCRIPTION

Helper methods for ResultSet classes that have a modified_utc column.

=head1 METHODS

=head2 sort_by_created ($direction = 'desc')

Sort the resultset by the created_utc column.

direction can be 'asc' or 'desc' and defaults to 'desc' (ie. newest first)

=head2 sort_by_modified ($direction = 'desc')

Sort the resultset by the modified_utc column.

direction can be 'asc' or 'desc' and defaults to 'desc' (ie. newest first)

=cut
