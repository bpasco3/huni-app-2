package HuNI::Backend::Schema::Result;

use 5.16.0;
use warnings;

use parent 'DBIx::Class::Core';
use Function::Parameters qw( :strict );
use JSON;

sub non_json_columns {
    return;
}

my %column_handler = (
    boolean   => fun ($value) { $value ? JSON::true : JSON::false },
    timestamp => fun ($value) { $value->strftime('%FT%T.%3N Z') },
);

method for_json() {
    # Get the basic hash of inflated values
    my $data = { $self->get_inflated_columns };
    delete @{ $data }{ $self->non_json_columns };

    # This contains the column metadata
    my $columns_info = $self->result_source->columns_info;

    for my $column (keys %$data) {
        my $info = $columns_info->{$column};
        next unless $info;  # virtual columns like record_count have no metadata

        # Fixup column values depending on type
        my $data_type = $info->{data_type};
        if (my $handler = $column_handler{$data_type}) {
            $data->{$column} = $handler->($data->{$column});
        }
    }

    return $data;
}

# This code is hacked from DBIx::Class::ResultSource::MultipleTableInheritance
method inherit_relationships_from($class: $source) {
    # If $source starts with a +, it's a full spec - remove the +
    # Otherwise, prepend HuNI::Backend::Schema::Result;
    if ($source =~ /^\+/) {
        $source =~ s/^\+//;
    }
    else {
        $source = __PACKAGE__ . '::' . $source;
    }

    Class::C3::Componentised->ensure_class_loaded($source);
    for my $rel ($source->relationships) {
        my $rel_info = $source->relationship_info($rel);

        $class->add_relationship(
            $rel, $rel_info->{source}, $rel_info->{cond},
            # extra key first to default it
            {originally_defined_in => 'Link', %{$rel_info->{attrs}}},
        );
    }
}

# This is a bit of a hack. Basically it tells us if $resultset_name got
# specified as a prefetch. What I really need is to be able to detect in a
# resultset whether a custom with_something method got called. This is the best
# I can manage at the moment. At least it's just in one place.
method has_related($resultset_name) {
    return exists $self->{related_resultsets}->{$resultset_name};
}

1;

__END__

=head1 NAME

HuNI::Backend::Schema::Result - base class for all Result classes

=head1 METHODS

=head2 for_json

Returns the current row as a hashref suitable for conversion to json.

=head2 non_json_columns

Override this to specify a list of columns that should be not returned by
for_json.

=cut
