package HuNI::Backend::Schema::ResultSet;

use 5.16.0;
use warnings;
use parent 'DBIx::Class::ResultSet';

use Data::Dumper::Concise;
use Function::Parameters qw( :strict );

method as_hashes() {
    $self->search({}, {
        result_class => 'DBIx::Class::ResultClass::HashRefInflator'
    });
}

method dump($fh = \*STDERR) {
    my @all = $self->as_hashes->all;
    print {$fh} Dumper(\@all);
}

method for_json() {
    return [ map { $_->for_json } $self->all ];
}

method prefetch($prefetches) {
    return $self->search({ }, { prefetch => $prefetches });
}

method with_columns($column_hash, $join_tables = [ ]) {
    return $self->search({ }, {
        '+select' => [ values %$column_hash ],
        '+as'     => [ keys   %$column_hash ],
        'join'    => $join_tables
    });
}

1;

__END__

=head1 NAME

HuNI::Backend::Schema::ResultSet

=head1 DESCRIPTION

This is the base class for all the HuNI::Backend::Schema resultsets.
Any methods defined here will be available for all resultsets.

=head1 METHODS

=head2 dump

Print out the resultset to STDERR or the provided file handle.

=head2 as_hashes

Returns the results as hashes rather than DBix:Class::Row objects.

=head2 for_json

Return the results as an arrayref of hashrefs, suitable for returning
directly from Dancer2 route handlers.

    eg.

    get '/search' => sub {
        my $rs = resultset('Search')->search({
            user_id => var('user')->{user_id}
        });
        return $rs->for_json;
    }

=head2 with_columns

Select some extra columns.

=over

=item $column_hash

The values form the C<+select> array, the keys are the C<+as>.

    eg. { role_id => 'collection_users.role_id' }

=item $join_tables

An optional arrayref of table names to join to.

=back

=cut
