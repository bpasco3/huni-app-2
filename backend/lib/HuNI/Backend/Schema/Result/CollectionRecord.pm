use utf8;
package HuNI::Backend::Schema::Result::CollectionRecord;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Backend::Schema::Result::CollectionRecord

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Backend::Schema::Result>

=cut

use base 'HuNI::Backend::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<huni.collection_record>

=cut

__PACKAGE__->table("huni.collection_record");

=head1 ACCESSORS

=head2 collection_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 docid

  data_type: 'text'
  is_nullable: 0

=head2 created_utc

  data_type: 'timestamp'
  default_value: timezone('utc'::text, now())
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "collection_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "docid",
  { data_type => "text", is_nullable => 0 },
  "created_utc",
  {
    data_type     => "timestamp",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 0,
  },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</collection_id>

=item * L</docid>

=back

=cut

__PACKAGE__->set_primary_key("collection_id", "docid");

=head1 RELATIONS

=head2 collection

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::Collection>

=cut

__PACKAGE__->belongs_to(
  "collection",
  "HuNI::Backend::Schema::Result::Collection",
  { id => "collection_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 user

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "HuNI::Backend::Schema::Result::User",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-05 03:48:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:6zmgWk5UY5UmW3loSh4C3A

use Role::Tiny::With;
with 'HuNI::Backend::Schema::Role::Result::Timestamps';

use Function::Parameters qw( :strict );

=head1 CUSTOM METHODS

=head2 non_json_columns

Returns a list of columns which get skipped when converting to json.

=cut

sub non_json_columns {
    qw( collection_id )
}

method for_json() {
    my $json = $self->SUPER::for_json;

    if ($self->has_related('user')) {
        $json->{username} = $self->user->name;
    }

    return $json;
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
