use utf8;
package HuNI::Backend::Schema::Result::Linktype;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Backend::Schema::Result::Linktype

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Backend::Schema::Result>

=cut

use base 'HuNI::Backend::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<huni.linktype>

=cut

__PACKAGE__->table("huni.linktype");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'huni.linktype_id_seq'

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 from_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 to_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 pair_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 created_utc

  data_type: 'timestamp'
  default_value: timezone('utc'::text, now())
  is_nullable: 0

=head2 modified_utc

  data_type: 'timestamp'
  default_value: timezone('utc'::text, now())
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "huni.linktype_id_seq",
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "from_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "to_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "pair_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "created_utc",
  {
    data_type     => "timestamp",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 0,
  },
  "modified_utc",
  {
    data_type     => "timestamp",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 from

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::Category>

=cut

__PACKAGE__->belongs_to(
  "from",
  "HuNI::Backend::Schema::Result::Category",
  { id => "from_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 links

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::Link>

=cut

__PACKAGE__->has_many(
  "links",
  "HuNI::Backend::Schema::Result::Link",
  { "foreign.linktype_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 linktypes

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::Linktype>

=cut

__PACKAGE__->has_many(
  "linktypes",
  "HuNI::Backend::Schema::Result::Linktype",
  { "foreign.pair_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pair

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::Linktype>

=cut

__PACKAGE__->belongs_to(
  "pair",
  "HuNI::Backend::Schema::Result::Linktype",
  { id => "pair_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 to

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::Category>

=cut

__PACKAGE__->belongs_to(
  "to",
  "HuNI::Backend::Schema::Result::Category",
  { id => "to_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 user

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "HuNI::Backend::Schema::Result::User",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-05 03:48:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:14ADtSCJpYL/qpW2eKKPiw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
