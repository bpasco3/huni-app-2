use utf8;
package HuNI::Backend::Schema::Result::Linkall;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Backend::Schema::Result::Linkall

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Backend::Schema::Result>

=cut

use base 'HuNI::Backend::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<huni.linkall>

=cut

__PACKAGE__->table("huni.linkall");
__PACKAGE__->result_source_instance->view_definition(" SELECT me.id,\n    me.user_id,\n    me.synopsis,\n    me.from_docid,\n    me.to_docid,\n    me.linktype_id,\n    me.created_utc,\n    me.modified_utc\n   FROM huni.link me\nUNION\n SELECT me.id,\n    me.user_id,\n    me.synopsis,\n    me.to_docid AS from_docid,\n    me.from_docid AS to_docid,\n    lt.pair_id AS linktype_id,\n    me.created_utc,\n    me.modified_utc\n   FROM (huni.link me\n     JOIN huni.linktype lt ON ((me.linktype_id = lt.id)))");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 1

=head2 user_id

  data_type: 'integer'
  is_nullable: 1

=head2 synopsis

  data_type: 'text'
  is_nullable: 1

=head2 from_docid

  data_type: 'text'
  is_nullable: 1

=head2 to_docid

  data_type: 'text'
  is_nullable: 1

=head2 linktype_id

  data_type: 'integer'
  is_nullable: 1

=head2 created_utc

  data_type: 'timestamp'
  is_nullable: 1

=head2 modified_utc

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 1 },
  "user_id",
  { data_type => "integer", is_nullable => 1 },
  "synopsis",
  { data_type => "text", is_nullable => 1 },
  "from_docid",
  { data_type => "text", is_nullable => 1 },
  "to_docid",
  { data_type => "text", is_nullable => 1 },
  "linktype_id",
  { data_type => "integer", is_nullable => 1 },
  "created_utc",
  { data_type => "timestamp", is_nullable => 1 },
  "modified_utc",
  { data_type => "timestamp", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-05 03:48:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kKV2ffwhgVqojJoGhq1BMg

__PACKAGE__->inherit_relationships_from('Link');

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
