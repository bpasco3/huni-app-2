use utf8;
package HuNI::Backend::Schema::Result::User;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Backend::Schema::Result::User

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Backend::Schema::Result>

=cut

use base 'HuNI::Backend::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<huni.user>

=cut

__PACKAGE__->table("huni.user");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'huni.user_id_seq'

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 email

  data_type: 'text'
  is_nullable: 1

=head2 institutions

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 interests

  data_type: 'text'
  is_nullable: 1

=head2 collaboration_interests

  data_type: 'text'
  is_nullable: 1

=head2 auth_userid

  data_type: 'text'
  is_nullable: 0

=head2 auth_domain

  data_type: 'text'
  is_nullable: 0

=head2 created_utc

  data_type: 'timestamp'
  default_value: timezone('utc'::text, now())
  is_nullable: 0

=head2 modified_utc

  data_type: 'timestamp'
  default_value: timezone('utc'::text, now())
  is_nullable: 0

=head2 api_username

  data_type: 'text'
  is_nullable: 1

=head2 api_hashed_key

  data_type: 'text'
  is_nullable: 1

=head2 api_hash_method

  data_type: 'text'
  is_nullable: 1

=head2 can_edit

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 show_profile_info

  data_type: 'boolean'
  is_nullable: 1

=head2 auth_extra

  data_type: 'json'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "huni.user_id_seq",
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "email",
  { data_type => "text", is_nullable => 1 },
  "institutions",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "interests",
  { data_type => "text", is_nullable => 1 },
  "collaboration_interests",
  { data_type => "text", is_nullable => 1 },
  "auth_userid",
  { data_type => "text", is_nullable => 0 },
  "auth_domain",
  { data_type => "text", is_nullable => 0 },
  "created_utc",
  {
    data_type     => "timestamp",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 0,
  },
  "modified_utc",
  {
    data_type     => "timestamp",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 0,
  },
  "api_username",
  { data_type => "text", is_nullable => 1 },
  "api_hashed_key",
  { data_type => "text", is_nullable => 1 },
  "api_hash_method",
  { data_type => "text", is_nullable => 1 },
  "can_edit",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "show_profile_info",
  { data_type => "boolean", is_nullable => 1 },
  "auth_extra",
  { data_type => "json", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<api_username_unique>

=over 4

=item * L</api_username>

=back

=cut

__PACKAGE__->add_unique_constraint("api_username_unique", ["api_username"]);

=head2 C<auth_userid_domain_unique>

=over 4

=item * L</auth_userid>

=item * L</auth_domain>

=back

=cut

__PACKAGE__->add_unique_constraint("auth_userid_domain_unique", ["auth_userid", "auth_domain"]);

=head1 RELATIONS

=head2 collection_records

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::CollectionRecord>

=cut

__PACKAGE__->has_many(
  "collection_records",
  "HuNI::Backend::Schema::Result::CollectionRecord",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 collection_user_inviting_users

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::CollectionUser>

=cut

__PACKAGE__->has_many(
  "collection_user_inviting_users",
  "HuNI::Backend::Schema::Result::CollectionUser",
  { "foreign.inviting_user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 collection_user_users

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::CollectionUser>

=cut

__PACKAGE__->has_many(
  "collection_user_users",
  "HuNI::Backend::Schema::Result::CollectionUser",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 contents

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::Content>

=cut

__PACKAGE__->has_many(
  "contents",
  "HuNI::Backend::Schema::Result::Content",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 links

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::Link>

=cut

__PACKAGE__->has_many(
  "links",
  "HuNI::Backend::Schema::Result::Link",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 linktypes

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::Linktype>

=cut

__PACKAGE__->has_many(
  "linktypes",
  "HuNI::Backend::Schema::Result::Linktype",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-15 04:08:47
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:pRTE0opfcRkIghlmdbMFjQ

__PACKAGE__->many_to_many(collections => 'collection_user_users', 'collection');

use Role::Tiny::With;
with 'HuNI::Backend::Schema::Role::Result::Timestamps';

use Crypt::URandom          qw( urandom );
use Function::Parameters    qw( :strict );
use PBKDF2::Tiny            qw( );

my %hasher_table = (
    # The plaintext hasher is only used in testing, to make sure we auto-upgrade
    # to the preferred_hasher in check_api_key.
    plaintext => {
        encode => sub { @_ },
        test   => sub { $_[0] eq $_[1] },
    },
    pbkdf2 => {
        encode => \&pbkdf2_encode,
        test   => \&pbkdf2_test,
    },
);
my $preferred_hasher = 'pbkdf2';
my $api_key_length = 32;

method generate_api_key() {
    # if username does not exist, create one
    if (! defined $self->api_username) {
        $self->generate_api_username;
    }

    my $api_key = unpack('h*', urandom($api_key_length));
    $self->update_api_key($api_key);
    return $api_key;
}

method check_api_key($api_key) {
    my $hasher = $hasher_table{$self->api_hash_method};

    return unless $hasher->{test}->($api_key, $self->api_hashed_key);

    if ($self->api_hash_method ne $preferred_hasher) {
        $self->update_api_key($api_key);
    }

    return 1;
}

method update_api_key($api_key) {
    my $hasher = $hasher_table{$preferred_hasher};

    $self->update({
        api_hashed_key   => $hasher->{encode}->($api_key),
        api_hash_method  => $preferred_hasher,
    });
}

method generate_api_username() {
    # We don't want to use auth_userid here, as that's not meant to be public.
    # The users may or may not have an email address.
    # Lets just give them a number.
    while (1) {
        my $username = rand_digits(8);
        return if eval { $self->update({ api_username => $username }) };
        # try again - TODO: time out?
    }
}

fun rand_digits($howmany) {
    my $lo = 10 ** ($howmany - 1);
    my $hi = 10 ** $howmany;
    return int(rand($hi - $lo)) + $lo;
}

my $pbkdf2_type         = 'SHA-1';
my $pbkdf2_iterations   = 1000;
my $pbkdf2_length       = $api_key_length;

fun pbkdf2_encode($api_key) {
    my $salt = urandom(8);
    # Note we need to force $api_key to be a string, in both derive and verify
    # Otherwise we get all sorts of argument "\\\\\\\\..." not numeric warnings
    my $dk = PBKDF2::Tiny::derive(
        $pbkdf2_type, "$api_key", $salt, $pbkdf2_iterations, $pbkdf2_length
    );
    return join(':', map { unpack('h*', $_) } ($salt, $dk));
}

fun pbkdf2_test($api_key, $hashed) {
    my ($salt, $dk) = map { pack('h*', $_) } split(/:/, $hashed);
    return PBKDF2::Tiny::verify(
        $dk, $pbkdf2_type, "$api_key", $salt, $pbkdf2_iterations, $pbkdf2_length
    );
}

sub non_json_columns {
    qw( api_username api_hashed_key api_hash_method )
}

1;
