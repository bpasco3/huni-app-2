use utf8;
package HuNI::Backend::Schema::Result::Category;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Backend::Schema::Result::Category

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Backend::Schema::Result>

=cut

use base 'HuNI::Backend::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<huni.category>

=cut

__PACKAGE__->table("huni.category");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'huni.category_id_seq'

=head2 name

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "huni.category_id_seq",
  },
  "name",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<unique_name>

=over 4

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("unique_name", ["name"]);

=head1 RELATIONS

=head2 linktypes_from

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::Linktype>

=cut

__PACKAGE__->has_many(
  "linktypes_from",
  "HuNI::Backend::Schema::Result::Linktype",
  { "foreign.from_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 linktypes_to

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::Linktype>

=cut

__PACKAGE__->has_many(
  "linktypes_to",
  "HuNI::Backend::Schema::Result::Linktype",
  { "foreign.to_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-05 03:48:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:MY3440FnhgA21V7VOSPhiw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
