package HuNI::Backend::Schema::ResultSet::CollectionRecord;

use 5.16.0;
use warnings;
use parent 'HuNI::Backend::Schema::ResultSet';
use Function::Parameters qw( :strict );
use Role::Tiny::With;

with 'HuNI::Backend::Schema::Role::ResultSet::Timestamps';

1;

__END__

=head1 NAME

HuNI::Backend::Schema::ResultSet::CollectionRecord

=cut
