package HuNI::Backend::Schema::ResultSet::AafJti;

use 5.16.0;
use warnings;
use parent 'HuNI::Backend::Schema::ResultSet';
use Function::Parameters qw( :strict );

method validate($jti, $expiry) {

    # First of all, let's clear out all the expired jti fields.
    $self->search({ expiry => { '<', time } })->delete;

    # Now, attempt to insert this jti value, failing if it already exists.
    return $self->create({ jti => $jti, expiry => $expiry });
}

1;

__END__

=head1 NAME

HuNI::Backend::Schema::ResultSet::AafJti

=head1 METHODS

=head2 validate($jti, $expiry)

Firstly deletes all expired JTI entries.
Then attempts to add the JTI and expiry to the table.
If the JTI already exists, this method will die, otherwise the row is returned.

=cut
