package HuNI::Backend::Schema::ResultSet::Linkall;

use 5.16.0;
use warnings;
use parent 'HuNI::Backend::Schema::ResultSet::Link';

1;

__END__

=head1 NAME

HuNI::Backend::Schema::ResultSet::Linkall

=head1 DESCRIPTION

The Linkall resultset just inherits everything from the Link resultset.

=cut
