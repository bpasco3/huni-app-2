package HuNI::Backend::Schema::ResultSet::Content;

use 5.16.0;
use warnings;
use parent 'HuNI::Backend::Schema::ResultSet';
use Function::Parameters qw( :strict );

method find_latest($id) {
    my $attr = {
        order_by => { -desc => 'version' },
        rows => 1,
    };
    return $self->search({ id => $id }, $attr)->single;
}

method insert_item(:$id, :$content, :$user_id) {
    my $version = 1;

    if (my $previous = $self->find_latest($id)) {
        if ($previous->content eq $content) {
            return $previous->version;
        }
        $version = $previous->version + 1;
    }

    $self->create({
        id      => $id,
        content => $content,
        user_id => $user_id,
        version => $version,
    });

    return $version;
}

method document_history($id) {
    return $self->search({ id => $id }, {
        columns => [qw( version created_utc )],
        order_by => { -desc => 'version' },
    });
}

1;

__END__

=head1 NAME

HuNI::Backend::Schema::ResultSet::Content

=cut
