package HuNI::Backend::Schema::ResultSet::Link;

use 5.16.0;
use warnings;
use parent 'HuNI::Backend::Schema::ResultSet';
use Function::Parameters qw( :strict );
use Role::Tiny::With;

with 'HuNI::Backend::Schema::Role::ResultSet::Timestamps';
with 'HuNI::Backend::Schema::Role::ResultSet::UserColumns';

method with_linktypes() {
    return $self->search(undef, {
        '+select' => [qw( linktype.name pair.name linktype.pair_id )],
        '+as'     => [qw( linktype      pairtype  pairtype_id      )],
        join      => [{ linktype => 'pair' }],
    });
}

method with_the_lot($user_id) {
    return $self->with_linktypes
                ->with_username
                ->with_is_mine($user_id);
}

1;

__END__

=head1 NAME

HuNI::Backend::Schema::ResultSet::Link

=head1 METHODS


=cut
