package HuNI::Backend::Schema::ResultSet::Collection;

use 5.16.0;
use warnings;
use parent 'HuNI::Backend::Schema::ResultSet';
use Function::Parameters qw( :strict );
use Role::Tiny::With;

with 'HuNI::Backend::Schema::Role::ResultSet::Timestamps';

method with_extra() {
    return $self->search({ }, { prefetch => 'extra' });
}

method with_records() {
    return $self->search({ }, {
        prefetch => { collection_records => 'user' }, # usernames too
        order_by => 'collection_records.created_utc',
    });
}

method for_user($user_id, $allowed_roles = [ ]) {
    my $query = {
        'collection_users.user_id' => $user_id,
    };
    $query->{'collection_users.role_id'} = $allowed_roles if @$allowed_roles;

    my $attr = {
        join => [qw( collection_users )],
    };

    # It only makes sense to include the role_id if we're talking about a
    # specific user, so we might as well include it here.
    return $self->search($query, $attr)
                ->with_columns({ role_id => 'collection_users.role_id' });
}

method viewable_by($user_id) {

    if (! defined($user_id)) {
        return $self->search({ is_public => 1 });
    }

    return $self->search(
        {
            -or => [
                is_public => 1,
                'collection_users.user_id' => $user_id,
            ],
        },
        {
            join => [qw( collection_users )],
        },
    );
}

1;

__END__

=head1 NAME

HuNI::Backend::Schema::ResultSet::Collection

=head1 METHODS



=cut
