package HuNI::Backend::SolrInterface;

use 5.20.0;
use warnings;

use Moo;
use Function::Parameters    qw( :strict );
use JSON                    qw( encode_json );
use HTTP::Tiny              qw( );

use namespace::clean;

has ua => (
    is => 'lazy',
    builder => sub { HTTP::Tiny->new },
);

has endpoint => (
    is => 'ro',
    required => 1,
);

method add($doc) {
    $self->_post([ $doc ]);
}

method commit() {
    $self->_post({ commit => { expungeDeletes => \1 } });
}

method optimise() {
    $self->_post({ optimize => { waitSearcher => \0 } });
}

method _post($content) {
    my $request = {
        content => encode_json($content),
        headers => {
            'content-type' => 'application/json',
        },
    };

    my $response = $self->ua->post($self->endpoint, $request);
    if ($response->{success}) {
#        use Data::Dumper::Concise; print STDERR Dumper($response);
    }
    else {
        use Data::Dumper::Concise; print STDERR Dumper($response);
        die "Solr request failed!";
    }
}

my %huni_solr_fields = (
    sourceAgencyCode    => 'HuNI',
    sourceSiteAddress   => 'https://huni.net.au/',
    sourceAgencyName    => 'HuNI',
    sourceSiteTag       => 'HuNI is a new research and discovery platform developed by and for humanities and creative arts scholars,',
);

method update_collection($c, $is_deleted = 0) {
    if (!$c->in_solr) {
        if ($is_deleted || !$c->is_public) {
            # This collection needs to be hidden/removed, but it's not in solr
            # anyway, so forget about it.
            return;
        }
    }

    my $state = $is_deleted     ? 'deleted'
              : $c->is_public   ? 'public'
              :                   'private';

    my $collection_id   = $c->id;
    my $docid           = "HuNI***Collection***$collection_id";
    my $uri             = $huni_solr_fields{sourceSiteAddress};
    my $collection_uri  = "${uri}vlab/collection/$collection_id";

    my $solr = {
        %huni_solr_fields,
        docid               => $docid,
        entityType          => 'Work',
        document_history    => [ $collection_uri ],
        provider_source     => $collection_uri,
        name                => $c->is_public ? $c->name : 'Private Collection',
    };

    if ($state eq 'public') {
        $solr->{type}               = 'Collection';
        $solr->{sourceRecordLink}   = "${uri}#/record/${docid}";
        $solr->{sourceID}           = $collection_id;
        $solr->{description}        = $c->synopsis if $c->synopsis;
    }
    else {
        $solr->{type} = "Collection ($state)";
    }

#use Data::Dumper::Concise; print STDERR Dumper({ SOLR => $solr }); return;

    $self->add($solr);
    $self->commit;

    $c->update({ in_solr => 1 });
}

1;
