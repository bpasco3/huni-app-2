package HuNI::Backend::WebApp::Util;

use 5.16.0;
use warnings;
use base 'Exporter';

our @EXPORT_OK = qw(
    neo4j           set_neo4j
    schema          set_schema
    solr_select     set_solr_select
    solr_update     set_solr_update

    lookup_users
    resultset
);

use HuNI::Backend::Schema;
use HuNI::Backend::SolrInterface;
use Function::Parameters qw( :strict );
use List::Util           qw( uniq );
use REST::Neo4j;
use REST::Solr;

my $neo4j;
my $schema;
my $solr_select;
my $solr_update;

fun set_neo4j($new_neo4j) {
    return $neo4j = $new_neo4j;
}

fun set_schema($new_schema) {
    return $schema = $new_schema;
}

fun set_solr_select($new_solr_select) {
    return $solr_select = $new_solr_select;
}

fun set_solr_update($new_solr_update) {
    return $solr_update = $new_solr_update;
}

sub neo4j {
    return $neo4j //= _create_neo4j();
}

sub schema {
    return $schema //= _create_schema();
}

sub solr_select {
    return $solr_select //= _create_solr_select();
}

sub solr_update {
    return $solr_update //= _create_solr_update();
}

fun resultset($name) {
    return schema()->resultset($name);
}

sub _create_neo4j {
    my $url = $ENV{SD_NEO4J_URI}
            // HuNI::Backend::WebApp::setting('neo4j')->{uri};
    $url =~ s{/$}{};
    return REST::Neo4j->new(base_url => "$url/db/data/");
}

sub _create_schema {
    my $dsn = $ENV{HUNI_BACKEND_DSN}
            // HuNI::Backend::WebApp::setting('database')->{dsn};
    return HuNI::Backend::Schema->connect($dsn);
}

sub _create_solr_select {
    my $base_url = $ENV{HUNI_BACKEND_SOLR_SELECT}
            // HuNI::Backend::WebApp::setting('solr_select')
            // die "Solr select endpoint not set.\n";
    return REST::Solr->new(base_url => $base_url);
}

sub _create_solr_update {
    my $endpoint = $ENV{HUNI_BACKEND_SOLR_UPDATE}
            // HuNI::Backend::WebApp::setting('solr_update')
            // die "Solr update endpoint not set.\n";
    return HuNI::Backend::SolrInterface->new(endpoint => $endpoint);
}

fun lookup_users($items, $user_id) {
    return unless @$items;

    my @user_ids = uniq map { $_->{user_id} } @$items;
    my @users = resultset('User')->search(
        { id => \@user_ids },
        { select => [qw( id name )] },
    )->as_hashes->all;
    my %username = map { $_->{id} => $_->{name} } @users;

    for (@$items) {
        $_->{is_mine} = $_->{user_id} == $user_id;
        $_->{user} = {
            id => $_->{user_id},
            name => $username{$_->{user_id}} // "User $_->{user_id}",
        };
    }
}

1;

__END__

=head1 NAME

HuNI::Backend::WebApp::Util - simple utils for use in the web app

=cut
