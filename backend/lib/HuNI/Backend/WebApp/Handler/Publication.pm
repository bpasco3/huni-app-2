package HuNI::Backend::WebApp::Handler::Publication;
use Dancer2::Plugin;
use Function::Parameters qw( :strict );

use HuNI::Backend::WebApp::Util qw( resultset );
with qw(
    HuNI::Backend::WebApp::Handler::Role::Abort
);

fun intval($x) {
    return $1 if defined $x && $x =~ /^(\d+)$/;
    return;
}

# get '/publication'
register fetch_publications => method($app) {
    my $query   = $self->app->request->params('query');
    my $where   = {
        'me.is_public' => 1,
    };
    if (exists $query->{q}) {
        my $q = '%' . $query->{q} . '%';
        $where->{-or} = [
            'me.name'     => { ilike => $q },
            'me.synopsis' => { ilike => $q },
        ],
    }
    my $paging = {
        page => intval($query->{p}) // 1,
        rows => intval($query->{n}) // 10,
    };

    my $rs = eval {
        # The order of the queries is important here.
        resultset('Collection')
            ->search($where, $paging)
            ->with_extra
            ->sort_by_modified
    };
    $self->abort(404) unless $rs;
    my $pager = $rs->pager;
    return {
        total => int($pager->total_entries), # is string by default :/
        publications => $rs->for_json,
    };
};

fun make_array($c) {
    return ref $c ? @$c : ( $c );
}

fun valid_id($s) {
    return $s =~ /^\d+$/;
}

# get '/publication/count?c=id&c=id&c=id'
register fetch_publication_counts => method($app) {
    my @ids = grep { valid_id($_) } make_array($self->params->{c});

    my $collections = resultset('Collection')
        ->search({ is_public => 1, id => \@ids })
        ->with_extra
        ->for_json;

    return { map { $_->{id} => $_ } @$collections };
};

# get '/publication/:id'
register fetch_publication_details => method($app) {
    my $publication_id = $self->params->{id};
    my $user_id =  $self->vars->{user}->{user_id};

    my $publication = eval {
        resultset('Collection')
            ->viewable_by($user_id)
            ->with_extra
            ->with_records
            ->find($publication_id)
    };
    $self->abort(404) unless $publication;
    return $publication->for_json;
};

register_plugin;

1;
