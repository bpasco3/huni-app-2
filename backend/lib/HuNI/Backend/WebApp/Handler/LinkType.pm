package HuNI::Backend::WebApp::Handler::LinkType;
use Dancer2::Plugin;
use Function::Parameters qw( :strict );

use strict;
use warnings;

my %valid_entity_type;

# Make this a function so we can add bogus types in testing.
fun add_valid_entity_type($type) {
    $valid_entity_type{$type} = 1;
}

add_valid_entity_type($_) for
    qw( Concept Event Organisation Person Place Work );

use HuNI::Backend::WebApp::Util qw( neo4j resultset );
use REST::Neo4j::Util qw( swap_values with_counter inflate_timestamps );
with qw(
    HuNI::Backend::WebApp::Handler::Role::Abort
    HuNI::Backend::WebApp::Handler::Role::GetFields
);

# get '/linktype/:from/:to'
register fetch_linktypes => method($app) {
    my $params = {
        from_type => $self->params->{from},
        to_type   => $self->params->{to},
    };

    my $result = neo4j->do(<<'...', parameters => $params);
MATCH (lt:Linktype { from:{ from_type }, to:{ to_type } })
RETURN lt
ORDER BY lt.name
...

    return [ map { inflate_timestamps($_->{row}->[0]) } @{ $result->{data} } ];
};

# post '/linktype'
register create_linktype => method($app) {
    my %fields = (
        user_id => $self->vars->{user}->{user_id},
        $self->get_fields(required => [qw( from to name pair )]),
    );
    $self->abort(404) if grep { $_ !~ /\w/ } values %fields;

    for (qw( from to )) {
        $self->abort(404) unless $valid_entity_type{ $fields{$_} };
    }

    my $is_symmetric = ($fields{from} eq $fields{to})
                     && ($fields{name} eq $fields{pair});

    my $linktype;

    with_counter(neo4j, 'Linktype', fun($transaction_id, $unique_id) {

        $fields{id} = $unique_id;
        $fields{pair_id} = $is_symmetric ? $unique_id : $unique_id + 1;

        my $create_linktype = <<'...';
MERGE (linktype:Linktype {
    from: { data }.from,
    to:   { data }.to,
    name: { data }.name,
    pair: { data }.pair
})
    ON CREATE SET linktype          = { data },
                  linktype.created  = timestamp(),
                  linktype.modified = timestamp()
RETURN linktype
...

        # Create the first linktype
        my $result = neo4j->do($create_linktype,
                               parameters => { data => \%fields });

        $linktype = $result->{data}->[0]->{row}->[0]; # we return this

        # If this linktype already exists it will have a different id to the
        # one we provided.
        my $link_exists = $linktype->{id} != $fields{id};

        # Create the pair linktype if necessary.
        if (!$is_symmetric && !$link_exists) {
            swap_values(\%fields, qw( from to          ));
            swap_values(\%fields, qw( name pair        ));
            swap_values(\%fields, qw( id   pair_id     ));
            neo4j->do($create_linktype, parameters => { data => \%fields });
        }

        # If we used the counter, update it.
        if (!$link_exists) {
            $unique_id += $is_symmetric ? 1 : 2;
        }

        return $unique_id;
    });

    return $linktype;
};

register_plugin;

1;
