package HuNI::Backend::WebApp::Handler::Login;
use Dancer2::Plugin;
use Function::Parameters qw( :strict );

use HuNI::Backend::WebApp::Util qw( resultset );

# The frontend calls this to check if we're logged in or not.
register is_logged_in => method($app) {
    return $self->vars->{user} // {};
};

register_plugin;

1;
