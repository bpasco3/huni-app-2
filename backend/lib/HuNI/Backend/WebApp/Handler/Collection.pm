package HuNI::Backend::WebApp::Handler::Collection;
use Dancer2::Plugin;
use Function::Parameters qw( :strict );

use strict;
use warnings;

use HuNI::Backend::WebApp::Util qw( resultset solr_update );
with qw(
    HuNI::Backend::WebApp::Handler::Role::Abort
    HuNI::Backend::WebApp::Handler::Role::GetFields
);

my $no_solr_update = $ENV{HUNI_BACKEND_NO_SOLR_UPDATE};

# get '/collection'
register fetch_collections => method($app) {
    my $user_id =  $self->vars->{user}->{user_id};

    my $ret = resultset('Collection')
                ->with_extra
                ->for_user($user_id)
                ->sort_by_modified
                ->for_json;

    return $ret;
};

# post '/collection'
register fetch_or_create_collection => method($app) {
    my $user_id =  $self->vars->{user}->{user_id};

    my $collection = eval { resultset('Collection')->create({
        $self->get_fields(
            required => [qw( name )],
            optional => [qw( synopsis )],
        )
    }) };
    $self->abort(404) unless $collection;
    $collection->add_to_users(
        {
            id => $user_id,
        },
        {
            inviting_user_id => $user_id,
            role_id => 'owner',
        });

    # Newly created collections won't have a record_count field, or a
    # timestamp, and must be reloaded from the database.
    $collection = resultset('Collection')
                    ->for_user($user_id)
                    ->with_extra
                    ->with_records
                    ->find($collection->id);

    return $collection->for_json;
};

# get '/collection/:id'
register fetch_collection_details => method($app) {
    my $user_id =  $self->vars->{user}->{user_id};
    my $collection_id = $self->params->{id};
    my $collection = eval {
        resultset('Collection')
            ->for_user($user_id)
            ->with_extra
            ->with_records
            ->find($collection_id);
    };
    $self->abort(404) unless $collection;
    return $collection->for_json;
};

# get '/collection/:id/record'
register fetch_collection_records => method($app) {
    my $user_id =  $self->vars->{user}->{user_id};
    my $collection_id = $self->params->{id};
    my $collection = eval {
        resultset('Collection')
            ->for_user($user_id)
            ->with_records
            ->find($collection_id);
    };
    $self->abort(404) unless $collection;
    return $collection->collection_records->sort_by_created->for_json;
};

# get '/collection/:id/user'
register get_users_for_collection => method($app) {
    my $user_id = $self->vars->{user}->{user_id};
    my $collection_id = $self->params->{id};

    my $users = eval {
        resultset('Collection')
            ->for_user($user_id, [qw( owner )])
            ->find($collection_id)
            ->collection_users
            ->prefetch([qw( user inviting_user )])
            ->for_json;
    };
say STDERR $@ if $@;
    $self->abort(404) unless $users;
    return $users;
};

# get '/collection/:id/suggest?q="parts of name"
register get_user_suggestions => method($app) {
    my $user_id = $self->vars->{user}->{user_id};
    my $collection_id = $self->params->{id};
    my $query = $self->app->request->params('query');

    # Break the search string on whitespace into separate chunks, and match
    # on all of them. eg. "te bu" would match "Ted Bullpit"
    my @string_matches = map { ( name => { ilike => "%$_%" } ) }
                            split(/\s+/, $query->{q});

    # The inner resultset contains users that this collection already has.
    # We exclude them from the results.
    my $existing_ids = resultset('CollectionUser')
                            ->search({ collection_id => $collection_id })
                            ->get_column('user_id');

    return resultset('User')
        ->search(
            {
                -and => [
                    auth_domain => { '!=' => 'huni' }, # system user
                    id => { -not_in => $existing_ids->as_query },
                    @string_matches,
                ],
            },
            {
                order_by => [qw( name )],
                select => [qw( id name )],
            },
          )->for_json;
};

fun intersection($as, $bs) {
    my %in_as = map { $_ => 1 } @$as;
    return [ grep { $in_as{$_} } @$bs ];
}

# put '/collection/:id
register set_collection_metadata => method($app) {
    my %required_roles = (
        is_public   => [qw( owner               )],
        name        => [qw( owner               )],
        synopsis    => [qw( owner read-write    )],
    );

    # Start with all roles allowed. The final result is the intersection of
    # all the required roles.
    my $roles = [qw( owner read-write read-only )];

    # Step through all the pairs in the request data, and
    #   1. check that the keys are valid keys (reject those that are not)
    #   2. collect the required roles
    my %update;
    my $req = $app->request->data;
    for my $key (keys %$req) {
        next unless exists $required_roles{$key};
        $roles = intersection($roles, $required_roles{$key});
        $update{$key} = $req->{$key};
    }

    my $user_id =  $self->vars->{user}->{user_id};
    my $collection_id = $self->params->{id};
    my $collection = eval {
        resultset('Collection')
            ->for_user($user_id, $roles)
            ->find($collection_id);
    };
    $self->abort(404) unless $collection;

    eval { $collection->update(\%update) };
    $self->abort(400) if $@;

    # Reload the collection from the database
    $collection = resultset('Collection')
                    ->for_user($user_id)
                    ->with_extra
                    ->with_records
                    ->find($collection_id);
    solr_update->update_collection($collection) unless $no_solr_update;
    return $collection->for_json;
};

# del '/collection/:id'
register delete_collection => method($app) {
    my $user_id = $self->vars->{user}->{user_id};
    my $collection_id = $self->params->{id};

    my $roles = [qw( owner )];
    my $collection = eval {
        resultset('Collection')
            ->for_user($user_id, $roles)
            ->find($collection_id);
    };
    $self->abort(404) unless $collection;

    solr_update->update_collection($collection, 1) unless $no_solr_update;
    $collection->delete;
    $self->status(200);
    return undef;
};

# put '/collection/:id/:docid'
register add_collection_document => method($app) {
    my $user_id = $self->vars->{user}->{user_id};
    my $collection_id = $self->params->{id};

    my $roles = [qw( owner read-write )];
    my $collection = eval {
        resultset('Collection')
            ->for_user($user_id, $roles)
            ->find($collection_id)
    };
    $self->abort(404) unless $collection;

    my %create = (
        docid => $self->params->{docid},
        user_id => $user_id,
    );
    if (!$collection->collection_records->find(\%create)) {
        my $row = eval { $collection->add_to_collection_records(\%create) };
        $self->abort(404) unless $row;
    }

    # Fetch the collection again to get the updated record list.
    return resultset('Collection')
            ->for_user($user_id)
            ->with_extra
            ->with_records
            ->find($collection_id)
            ->for_json;
};

# del '/collection/:id/:docid'
register delete_collection_document => method($app) {
    my $user_id = $self->vars->{user}->{user_id};
    my $collection_id = $self->params->{id};

    my $roles = [qw( owner read-write )];
    my $collection = eval {
        resultset('Collection')
            ->for_user($user_id, $roles)
            ->find({ id => $collection_id })
    };
    $self->abort(404) unless $collection;

    $collection->collection_records->search({
        docid => $self->params->{docid},
    })->delete;

    # Fetch the collection again to get the updated record list.
    return resultset('Collection')
            ->for_user($user_id)
            ->with_extra
            ->with_records
            ->find($collection_id)
            ->for_json;
};

# put '/collection/:id/user/:user_id/:role_id'
register add_user_to_collection => method($app) {
    my $inviting_user_id = $self->vars->{user}->{user_id};
    my $collection_id    = $self->params->{id};
    my $invited_user_id  = $self->params->{user_id};
    my $role_id          = $self->params->{role_id};

    # To invite someone, you must be an owner on that collection.
    my $owner_role = resultset('CollectionUser')->search({
        collection_id => $collection_id,
        user_id => $inviting_user_id,
        role_id => 'owner',
    })->single;
say STDERR $@ if $@;
    $self->abort(404) unless $owner_role;

    my $invited_role = eval { resultset('CollectionUser')->update_or_create({
        collection_id       => $collection_id,
        inviting_user_id    => $inviting_user_id,
        user_id             => $invited_user_id,
        role_id             => $role_id,
    }) };
say STDERR $@ if $@;
    $self->abort(404) unless $invited_role;

    return resultset('CollectionUser')->search({
        collection_id => $collection_id,
        user_id => $invited_user_id,
    })->prefetch([qw( user inviting_user )])->first->for_json;
};

# delete '/collection/:id/user/:user_id'
register delete_user_from_collection => method($app) {
    my $calling_user_id = $self->vars->{user}->{user_id};
    my $delete_user_id  = $self->params->{user_id};
    my $collection_id   = $self->params->{id};

    # To delete someone, you must be an owner on that collection.
    my $owner_role = resultset('CollectionUser')->search({
        collection_id   => $collection_id,
        user_id         => $calling_user_id,
        role_id         => 'owner',
    })->single;
    $self->abort(404) unless $owner_role;

    my $role_to_delete;

    if ($calling_user_id == $delete_user_id) {

        # You can delete yourself, but only if there's other owners.
        my $owner_count = resultset('CollectionUser')->search({
            collection_id   => $collection_id,
            role_id         => 'owner',
        })->count;
        $self->abort(404) unless $owner_count > 1;

        $role_to_delete = $owner_role;
    }
    else {
        $role_to_delete = resultset('CollectionUser')->search({
            collection_id   => $collection_id,
            user_id         => $delete_user_id,
        })->single;
        $self->abort(404) unless $role_to_delete;
    }

    $role_to_delete->delete;

    $self->status(200);
    return undef;
};

# delete '/collection/:id/user
register delete_self_from_collection => method($app) {
    my $collection_id   = $self->params->{id};
    my $user_id = $self->vars->{user}->{user_id};

    my $role = resultset('CollectionUser')->search({
        collection_id   => $collection_id,
        user_id         => $user_id,
    })->single;
    $self->abort(404) unless $role;

    if ($role->role_id eq 'owner') {
        # Owners can only delete themselves if there are other owners.
        # There must be at least one owner on a collection.
        my $owner_count = resultset('CollectionUser')->search({
            collection_id   => $collection_id,
            role_id         => 'owner',
        })->count;
        $self->abort(404) unless $owner_count > 1;
    }

    $role->delete;

    $self->status(200);
    return undef;
};

register_plugin;

1;
