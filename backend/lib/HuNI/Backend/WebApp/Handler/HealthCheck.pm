package HuNI::Backend::WebApp::Handler::HealthCheck;
use Dancer2::Plugin;
use Function::Parameters qw( :strict );
use Try::Tiny;

use HuNI::Backend::WebApp::Util qw( neo4j resultset solr_select );
with qw(
    HuNI::Backend::WebApp::Handler::Role::Abort
);

# get '/healthcheck'
register health_check => method($app) {
    my $ret = { };

    try {
        $ret->{users} = resultset('User')->count;
        my $collections = resultset('Collection')->search({ },
            {
                select   => ['is_public', { count => 'is_public' }],
                as       => [qw( is_public count )],
                group_by => [qw( is_public )],

            });
        while (my $row = $collections->next) {
            my $field = $row->is_public ? 'public' : 'private';
            my $count = $row->get_column('count');

            $ret->{collections}->{$field}  = $count;
        }
        $ret->{collections}->{records} = resultset('CollectionRecord')->count;

        $ret->{database}->{postgres} = 'ok';
    }
    catch {
        $ret->{database}->{postgres} = 'unavailable';
    };

    try {
        my $result = neo4j->do(<<'...');
MATCH (:Entity)-[link:Link]->(:Entity)
RETURN count(link) as links
...

        $ret->{links} = $result->{data}->[0]->{row}->[0];
        $ret->{database}->{neo4j} = 'ok';
    }
    catch {
        $ret->{database}->{neo4j} = 'unavailable';
    };

    try {
        $ret->{records} = solr_select->record_count;
        $ret->{database}->{solr} = 'ok';
    }
    catch {
        $ret->{database}->{solr} = 'unavailable';
    };

    $ret->{git} = $ENV{GIT_SHA} // 'unavailable';

    return $ret;
};

register_plugin;

1;
