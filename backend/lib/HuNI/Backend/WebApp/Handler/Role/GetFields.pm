package HuNI::Backend::WebApp::Handler::Role::GetFields;
use 5.16.0;
use warnings;
use Moo::Role;
use Function::Parameters qw( :strict );

requires 'abort';

method get_fields(:$required = [ ], :$optional = [ ]) {
    my $data = $self->app->request->data;
    if (!$data) {
        $self->abort if @$required;
        return; # no required params, so just return
    }

    my %field;
    for my $key (@$required) {
        $self->abort unless exists $data->{$key};
        $field{$key} = $data->{$key};
    }

    for my $key (@$optional) {
        $field{$key} = $data->{$key} if exists $data->{$key};
    }

    return %field;
}

method get_field($key) {
    my $data = $self->app->request->data;
    return $data->{$key} if $data && exists $data->{$key};
    $self->abort;
}

1;
