package HuNI::Backend::WebApp::Handler::Profile;
use Dancer2::Plugin;
use Function::Parameters qw( :strict );

use HuNI::Backend::WebApp::Util qw( resultset );
with qw(
    HuNI::Backend::WebApp::Handler::Role::Abort
    HuNI::Backend::WebApp::Handler::Role::GetFields
);

# Fetch the user profile from the database
# get '/profile'
register fetch_profile => method($app) {
    my $user_id = $self->vars->{user}->{user_id};
    my $user    = resultset('User')->find({ id => $user_id });
    return $user->for_json;
};

# Write the user data back to the database
# put '/profile'
register update_profile => method($app) {
    my $user = resultset('User')->find({
        id => $self->vars->{user}->{user_id}
    });
    $self->abort unless $user;

    # Scrub the incoming data to contain only the fields we want to update.
    my @fields = qw( name email institutions description
                     interests collaboration_interests show_profile_info );
    my %update = $self->get_fields(optional => \@fields);
    my $row = eval { $user->update(\%update) };
    $self->abort(404) unless $row;

    return $user->refresh_timestamps->for_json;
};

# Generate a new api key for the user and
# post '/apikey'
register generate_api_key => method($app) {
    my $user = resultset('User')->find({
        id => $self->vars->{user}->{user_id}
    });
    $self->abort unless $user;

    # Generate the api key for the user. This is the only time we see it in
    # plaintext.
    my $api_key = $user->generate_api_key;

    # Return the api key and username.
    return {
        api_key      => $api_key,
        api_username => $user->api_username,
    };
};

# get '/profile/find?u=userid&u=userid
register find_profiles => method($app) {
    my $query = $self->app->request->params('query');
    return [ ] unless exists $query->{u};

    my $query = { id => $query->{u} };
    my $attr  = { order_by => 'id' };
    my $rs = resultset('User')->search($query, $attr);

    # TODO: when we add the public/private field, add extra field here for
    # public profiles as appropriate
    my @profiles;
    while (my $row = $rs->next) {
        push(@profiles, {
            user_id => $row->id,
            username => $row->name,
        });
    }

    return \@profiles;
};

# get '/profile/:id'
register fetch_profile_for_user => method($app) {
    my $user_id = $self->params->{id};
    my $self_id = $self->vars->{user}->{user_id} // -1;

    my $user = eval { resultset('User')->find($user_id) };
    $self->abort(404) unless $user;

    my @public_fields = qw(
        id name show_profile_info
    );
    my @private_fields = qw(
        email institutions description
        interests collaboration_interests
    );

    if ($user->show_profile_info || ($user_id eq $self_id)) {
        push(@public_fields, @private_fields);
    }

    my %json;
    for my $field (@public_fields) {
        $json{$field} = $user->$field;
    }

    return \%json;
};

register_plugin;

1;
