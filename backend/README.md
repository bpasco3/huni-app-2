# Getting started

### 1. Install missing CPAN dependencies

        cpanm App::Sqitch Dancer2::Session::PSGI Data::GUID

These will be added to Strategic Perl.

### 2. Setup your shell environment

        source DEV-SETUP.env

This sets up your PERL5LIB path, and also the appropriate PostgreSQL environment
variables to access the default sqitch database.

### 3. Create and deploy a database

        createdb
        sqitch deploy


# Testing

HuNI::Backend uses Test::PostgreSQL to create fresh database instances.

Doing this for each test can be slow, so it is possible to pre-create one
of these.

Run this to start the test database server:

        ./tools/start-test-db

Then just use prove as normal.


# Running the server

There is a goserver alias in DEV-SETUP.env which will spawn the server on port
3001

Run this to start the server:

        goserver

You can force a mock user to be logged in with the HUNI_BACKEND_MOCK_USER environment variable:

        HUNI_BACKEND_MOCK_USER="Mr Fake User" goserver

This allows manual testing with tools like curl or wget.

# Using the OAuth2 logins in development mode

There are a set of development OAuth2 accounts which expect to be called
from http://localhost:3000/.

We can still run the server from anywhere, we just need to create an ssh tunnel.

Run this on your local machine.

        ssh $SERVER_HOST -f -L 3000:localhost:$SERVER_PORT -N

Then connect to [http://localhost:3000/](http://localhost:3000/) in your browser.

# Updating the schema

HuNI::Backend uses the following technologies for the database:

* PostgresSQL for the database itself
* sqitch to manage the DDL updates
* DBIx::Class for the ORM
* DBIx::Class::Schema::Loader to generate the ORM classes

There is a very good [tutorial for using sqitch with PostgreSQL](https://metacpan.org/pod/sqitchtutorial)

For example, let's say we are going to be adding a new column 'phone' to the
user table.

### 1. Create a new sqitch change called "user-phone" with:

        sqitch add user-phone -r user-table -n 'Add phone column to user'

### 2. Notice that three new files have been created:

        sqitch.d/deploy/user-phone.sql
        sqitch.d/verify/user-phone.sql
        sqitch.d/revert/user-phone.sql

### 3. Edit these files as per the sqitch tutorial.

### 4. Apply the change to the database:

        sqitch deploy

### 5. Examine the database and make sure everything looks right.

If the deploy fails, go back to step three. If the deploy succeeds but you still
aren't happy, use the following command to revert the latest sqitch change.

        sqitch revert @HEAD^

### .6 Re-generate the DBIC schema.

        ./tools/update-dbic-schema

### 7. Check the updated HuNI::Backend::Schema::Result classes to make sure everything is as expected.

Repeat steps 3 to 7 as necessary until you're happy with the change.

### 8. Commit the sqitch.d changes and the update DBIC code.
