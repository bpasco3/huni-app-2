use inc::Module::Install;

# Define metadata
name           'HuNI-Backend';
all_from       'lib/HuNI/Backend/WebApp.pm';

# Specific dependencies
#no_index       'directory'   => 'demos';
#install_script 'myscript';

tests_recursive;

Makefile->postamble(<<'END');
cover:
	cover -test 2>&1 | grep -v '(unknown origin)'

END

WriteAll;
