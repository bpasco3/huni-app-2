#!/usr/bin/env perl

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use HuNI::Backend::Schema   qw( );
use Log::Any::Adapter       qw( Stderr );
use REST::Neo4j             qw( );
use REST::Neo4j::Schema     qw( );
use REST::Solr              qw( );

main();
exit 0;

#--------

sub main {
    my $dsn = "dbi:Pg:host=$ENV{PGHOST};dbname=$ENV{POSTGRES_DB};user=$ENV{POSTGRES_USER};password=$ENV{POSTGRES_PASSWORD}";
    #my $dsn = 'dbi:Pg:host=dev02.sdt.name;dbname=huni-db;user=huni-postgres-user;password=huni-password;port=2004';
    my $pg = HuNI::Backend::Schema->connect($dsn);
    my $neo = REST::Neo4j->new(base_url => $ENV{SD_NEO4J_URI} . 'db/data/');
    my $solr = REST::Solr->new(base_url => $ENV{HUNI_BACKEND_SOLR_SELECT});
    #my $solr = REST::Solr->new(base_url => 'https://huni.net.au/solr/select/');
    #

    my $schema = REST::Neo4j::Schema->new(neo => $neo, config => 'neo4j-schema.yaml');

    $schema->deploy;
    load_linktypes($pg, $neo);

    my $entity = load_solr_documents($solr, get_docids($pg));
    load_links($pg, $entity, $neo);
}

fun load_links($pg, $entity, $neo) {
    my $create_links = <<'...';
    UNWIND { links } as linkdata
    MERGE (lhs:Entity { docid: linkdata.lhs.docid })
        ON CREATE SET lhs = linkdata.lhs
        ON MATCH  SET lhs = linkdata.lhs
    MERGE (rhs:Entity { docid: linkdata.rhs.docid })
        ON CREATE SET rhs = linkdata.rhs
        ON MATCH  SET rhs = linkdata.rhs
    MERGE (lhs)-[link:Link { linktype_id: linkdata.link.linktype_id }]->(rhs)
        ON CREATE SET link = linkdata.link
        ON MATCH  SET link = linkdata.link
...

    my $create_counter =<<'...';
    MATCH ()-[link:Link]-()
    WITH max(link.id) + 1 as nextId
    MERGE (counter:Counter { id:"Link" })
    SET counter.value = nextId
...

    my $i = 0;
    my $links = $pg->resultset('Link')->search(undef, {
        order_by => 'me.id',
        prefetch => [ { linktype => [qw( pair )] } ],
    });
    local $| = 1;
    my $total = $links->count;
    print "Adding $total links\n0 ";
    my @batch;
    my $batchsize = 250;
    my $progress = 0;
    my $send_batch = sub {
        my $result = $neo->do($create_links, parameters => {
            links => \@batch,
        });
        $progress += @batch;
        @batch = ();
        print "\r$progress ";
    };
    while (my $row = $links->next) {
        my $link = make_link($row, $entity);
        push(@batch, $link);
        $send_batch->() if @batch >= $batchsize;
    }
    $send_batch->() if @batch;
    print "\n";

    $neo->do($create_counter);
}

fun get_docids($pg) {
    my %docid;

    my $links = $pg->resultset('Link');
    while (my $row = $links->next) {

        $docid{ $row->from_docid }++;
        $docid{ $row->to_docid }++;
    }

    return [ sort keys %docid ];
}

sub trim {
    $_[0] =~ s/^\s*//;
    $_[0] =~ s/\s*$//;
}

fun trim_values($hash) {
    trim($_) for values %$hash;
}

fun load_solr_documents($solr, $docids) {
    my %doc;

    my $total = @$docids;
    local $| = 1;
    print "Loading $total solr documents\n0 ";
    my $batchsize = 250;
    while (@$docids) {
        my $batch = $solr->find_docids(splice(@$docids, 0, $batchsize));
        @doc{ keys %$batch } = values %$batch;

        my $progress = scalar keys %doc;
        print "\r$progress ";
    }

    trim_values($_) for values %doc;

    print "\n";
    return \%doc;
}

fun make_link($row, $entity) {
    my %link;

    $link{lhs}  = $entity->{$row->from_docid};
    $link{rhs}  = $entity->{$row->to_docid};
    $link{link} = { $row->get_columns };
    $link{link}->{$_} = $row->$_->epoch for qw( created_utc modified_utc );
    $link{link}->{linktype}    = $row->linktype->name;
    $link{link}->{pairtype}    = $row->linktype->pair->name;
    $link{link}->{pairtype_id} = $row->linktype->pair_id;

    return \%link;
}

fun load_linktypes($pg, $neo) {
    my $rs = $pg->resultset('Linktype')->search({ }, {
        prefetch => [qw( from to )],
    });

    my $create_linktypes = <<'...';
    UNWIND { linktypes } as data
    MERGE (linktype:Linktype { id: data.id })
        ON CREATE SET linktype = data
        ON MATCH  SET linktype = data
...

    my $create_counter =<<'...';
    MATCH (linktype:Linktype)
    WITH max(linktype.id) + 1 as nextId
    MERGE (counter:Counter { id:"Linktype" })
    SET counter.value = nextId
...

    my $total = $rs->count;
    local $| = 1;
    print "Adding $total link types\n0 ";
    my $progress = 0;
    my $batchsize = 50;
    my @batch;
    my $send_batch = sub {
        my $result = $neo->do($create_linktypes, parameters => {
            linktypes => \@batch,
        });
        $progress += @batch;
        @batch = ();
        print "\r$progress ";
    };
    while (my $row = $rs->next) {
        my %linktype = $row->get_columns;

        $linktype{from} = $row->from->name;
        $linktype{to}   = $row->to->name;
        $linktype{pair} = $row->pair->name;
        $linktype{$_} = $row->$_->epoch for qw( created_utc modified_utc );

        push(@batch, \%linktype);
        $send_batch->() if @batch >= $batchsize;
    }
    $send_batch->() if @batch;
    print "\n";

    $neo->do($create_counter);
}
