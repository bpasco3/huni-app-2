#!/usr/bin/env perl

use 5.22.0;
use warnings;

use Cpanel::JSON::XS        qw( );
use Function::Parameters    qw( :strict );
use HTTP::Tiny              qw( );
use Path::Tiny              qw( path );
use URI::Escape             qw( uri_escape );

my $production_solr         = 'https://huni.net.au/solr/select';
my $ua                      = HTTP::Tiny->new;
my $json                    = Cpanel::JSON::XS->new->utf8;

my $dev_solr_update = $ENV{HUNI_BACKEND_SOLR_UPDATE}
    or die "HUNI_BACKEND_SOLR_UPDATE not set - are you running in docker?\n";

if ((@ARGV == 0) || !($ARGV[0] > 0)) {
    die "usage: $0 <record-count> [provider...]\n";
}

main(@ARGV);
exit 0;

fun load_named_records($filename) {
    my @docids = map { quotemeta($_) } path($filename)->lines({ chomp => 1 });
    my $q = join(' OR ', @docids);
    my $records = record_query({ q => "docid:($q)", rows => scalar @docids });
    add_records($records, 1);
}

fun main($records_per_provider, @wanted_providers) {
    my $providers //= fetch_provider_list();
    if (@wanted_providers) {
        my %wanted = map { lc($_) => 1 } @wanted_providers;
        $providers = [ grep { $wanted{ lc($_->{name}) } } @$providers ];
    }

    for my $p (@$providers) {
        my $records = get_provider_records($p->{name}, $records_per_provider, $p->{count});
        add_records($records);
    }
    say STDERR "Committing";
    add_records([ ], 1);
}

fun fetch_provider_list() {
    my $providers = facet_query('sourceAgencyCode');
    return [
        map { +{ name => $_, count => $providers->{$_} } }
        grep { $_ ne 'HuNI' } # no huni collections please
            sort keys %$providers
    ];
}

fun get_provider_records($provider, $count, $total) {
    $count = $total if $count > $total;
    my $start = int(rand($total - $count + 1));
    say STDERR sprintf("%-9s $start/$count", "$provider:");

    return record_query({
        q               => 'text:*',
        fq              => 'sourceAgencyCode:' . $provider,
        rows            => $count,
        start           => $start,
    });
}

fun add_records($records, $commit = 0) {
    my $url = $dev_solr_update;
    if ($commit) {
        $url .= '?commit=true&optimize=true';
    }
    my $response = $ua->post($url, {
        headers => {
            'Content-Type' => 'application/json',
        },
        content => $json->encode($records),
    });

    if (!$response->{success}) {
        use Data::Dumper::Concise; print STDERR Dumper($response);
        die;
    }

    return 1;
}

fun record_query($query) {
    my $content = solr_query($query);
    my $records = $content->{response}->{docs};

    # Strip out internal and synthetic copyfields.
    for my $record (@$records) {
        delete $record->{_version_};
        delete $record->{startDate};
        delete $record->{endDate};
    }

    return $records;
}

fun facet_query($field) {
    my $content = solr_query({
        q               => 'text:*',
        rows            => 0,
        facet           => 'true',
        'facet.limit'   => -1,
        'facet.field'   => $field,
    });

    return { @{ $content->{facet_counts}->{facet_fields}->{$field} } };
}

fun solr_query($args) {
    my $query = make_query({ %$args, wt => 'json' });
    my $response = $ua->get($production_solr . '?' . $query);
    if (!$response->{success}) {
        use Data::Dumper::Concise; print STDERR Dumper($response);
        die;
    }

    return $json->decode($response->{content});
}

fun make_query($args) {
    return join('&',
        map { uri_escape($_) . '=' . uri_escape($args->{$_}) } keys %$args
    );
}
