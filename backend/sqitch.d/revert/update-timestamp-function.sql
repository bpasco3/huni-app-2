-- Revert update-timestamp-function

BEGIN;

DROP FUNCTION IF EXISTS huni.update_timestamp();

COMMIT;
