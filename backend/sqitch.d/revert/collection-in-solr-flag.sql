-- Revert collection-in-solr-flag

BEGIN;

ALTER TABLE huni.collection
    DROP COLUMN IF EXISTS in_solr;

COMMIT;
