-- Revert huni-backend:content-table from pg

BEGIN;

DROP TABLE IF EXISTS huni.content;

COMMIT;
