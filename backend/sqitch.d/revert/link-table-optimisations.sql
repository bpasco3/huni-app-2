-- Revert link-table-optimisations

BEGIN;

-- Remove the new trigger.
DROP TRIGGER IF EXISTS check_reverse_link_unique ON huni.link;

-- Reinstate the old trigger.
CREATE TRIGGER check_linkpair_unique
    BEFORE INSERT OR UPDATE ON huni.link
    FOR EACH ROW EXECUTE PROCEDURE huni.check_linkpair_unique();

-- Remove the new function.
DROP FUNCTION IF EXISTS huni.check_reverse_link_unique();

-- Remove the unique constraint.
ALTER TABLE huni.link DROP CONSTRAINT IF EXISTS forwards_unique;

COMMIT;
