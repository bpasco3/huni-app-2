-- Revert huni-backend:user-can-edit-column from pg

BEGIN;

ALTER TABLE huni.user DROP COLUMN IF EXISTS can_edit;

COMMIT;
