-- Revert huni-backend:drop-collection-user-column from pg

BEGIN;

ALTER TABLE huni.collection
 ADD COLUMN user_id INTEGER REFERENCES huni.user(id)
                    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;

UPDATE huni.collection c
   SET user_id = u.owner_id
  FROM (
    SELECT collection_id, min(user_id) owner_id
      FROM huni.collection_user
     WHERE role_id = 'owner'
  GROUP BY collection_id
    ) u
 WHERE u.collection_id = c.id;

ALTER TABLE huni.collection
    ALTER COLUMN user_id SET NOT NULL;

COMMIT;
