-- Revert linktype-table

BEGIN;

DROP FUNCTION IF EXISTS huni.create_linktype(TEXT, TEXT, TEXT, TEXT, INTEGER);
DROP FUNCTION IF EXISTS huni.create_linktype(TEXT, TEXT, TEXT, INTEGER);
DROP FUNCTION IF EXISTS huni.create_linktype(TEXT, TEXT, INTEGER);
DROP TABLE IF EXISTS huni.linktype;

COMMIT;
