-- Revert collection-timestamp-function

BEGIN;

DROP FUNCTION IF EXISTS huni.update_collection_timestamp();

COMMIT;
