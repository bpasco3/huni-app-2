-- Revert collection-modify-timestamp

BEGIN;

DROP TRIGGER IF EXISTS update_timestamp ON huni.collection_record;

COMMIT;
