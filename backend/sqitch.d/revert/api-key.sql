-- Revert api-key

BEGIN;

ALTER TABLE huni.user DROP CONSTRAINT IF EXISTS api_username_unique;

ALTER TABLE huni.user DROP COLUMN IF EXISTS api_username;
ALTER TABLE huni.user DROP COLUMN IF EXISTS api_hashed_key;
ALTER TABLE huni.user DROP COLUMN IF EXISTS api_hash_method;

COMMIT;
