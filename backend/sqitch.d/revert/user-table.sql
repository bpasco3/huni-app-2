-- Revert user-table

BEGIN;

DROP FUNCTION IF EXISTS huni.system_user_id();

DROP TABLE IF EXISTS huni.user;

COMMIT;
