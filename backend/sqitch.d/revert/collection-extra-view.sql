-- Revert huni-backend:collection-extra-view from pg

BEGIN;

DROP VIEW IF EXISTS huni.collection_extra;

COMMIT;
