-- Revert user-table-timestamp

BEGIN;

DROP TRIGGER IF EXISTS update_timestamp ON huni.user;

COMMIT;
