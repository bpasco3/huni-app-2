-- Deploy linkall-view
-- requires: link-table

BEGIN;

CREATE VIEW huni.linkall
    (id, user_id, synopsis, from_docid, to_docid, linktype_id, created_utc, modified_utc )
  AS
    SELECT me.id, me.user_id, me.synopsis, me.from_docid, me.to_docid,
           me.linktype_id, me.created_utc, me.modified_utc
    FROM huni.link me
  UNION
    SELECT me.id, me.user_id, me.synopsis, me.to_docid, me.from_docid,
           lt.pair_id, me.created_utc, me.modified_utc
    FROM huni.link me JOIN huni.linktype lt
        ON me.linktype_id = lt.id;

COMMIT;
