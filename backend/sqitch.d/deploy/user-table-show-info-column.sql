-- Deploy huni-backend:user-table-show-info-column to pg
-- requires: user-table

BEGIN;

-- We let the user choose whether to show their info. By default it is not
-- shown. The NULL value is treated like false, but also that the user should
-- be prompted to make a choice.
ALTER TABLE huni.user
  ADD COLUMN show_profile_info BOOLEAN DEFAULT NULL;

COMMIT;
