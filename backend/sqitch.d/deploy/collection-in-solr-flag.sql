-- Deploy collection-in-solr-flag
-- requires: collection-table

BEGIN;

ALTER TABLE huni.collection
    ADD COLUMN in_solr bool DEFAULT FALSE;

COMMIT;
