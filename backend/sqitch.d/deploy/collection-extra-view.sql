-- Deploy huni-backend:collection-extra-view to pg
-- requires: collection-user-table

BEGIN;

CREATE VIEW huni.collection_extra AS
     SELECT c.id                         as collection_id,
            coalesce(cs.record_count, 0) as record_count,
            coalesce(co.owners,  '[]')   as owners,
            coalesce(ce.editors, '[]')   as editors,
            coalesce(cv.viewers, '[]')   as viewers
       FROM huni.collection c
  LEFT JOIN (
        -- record_count
         SELECT collection_id,
                count(*) as record_count
           FROM huni.collection_record
       GROUP BY collection_id
            ) cs
         ON c.id = cs.collection_id
  LEFT JOIN (
        -- owners
       SELECT cu.collection_id,
              json_agg(
                json_build_object('id', cu.user_id, 'name', u.name)
                ORDER BY u.name
              ) as owners
         FROM huni.collection_user cu
         JOIN huni.user u
           ON cu.user_id = u.id
          AND cu.role_id = 'owner'
     GROUP BY cu.collection_id
            ) co
         ON c.id = co.collection_id
  LEFT JOIN (
        -- editors
       SELECT cu.collection_id,
              json_agg(
                json_build_object('id', cu.user_id, 'name', u.name)
                ORDER BY u.name
              ) as editors
         FROM huni.collection_user cu
         JOIN huni.user u
           ON cu.user_id = u.id
          AND cu.role_id = 'read-write'
     GROUP BY cu.collection_id
            ) ce
         ON c.id = ce.collection_id
  LEFT JOIN (
        -- viewers
       SELECT cu.collection_id,
              json_agg(
                json_build_object('id', cu.user_id, 'name', u.name)
                ORDER BY u.name
              ) as viewers
         FROM huni.collection_user cu
         JOIN huni.user u
           ON cu.user_id = u.id
          AND cu.role_id = 'read-only'
     GROUP BY cu.collection_id
            ) cv
         ON c.id = cv.collection_id
          ;

COMMIT;
