-- Deploy api-key
-- requires: user-table

BEGIN;

-- These can all be none.
ALTER TABLE huni.user ADD COLUMN api_username    text DEFAULT NULL;
ALTER TABLE huni.user ADD COLUMN api_hashed_key  text DEFAULT NULL;
ALTER TABLE huni.user ADD COLUMN api_hash_method text DEFAULT NULL;

ALTER TABLE huni.user ADD CONSTRAINT api_username_unique UNIQUE(api_username);

COMMIT;
