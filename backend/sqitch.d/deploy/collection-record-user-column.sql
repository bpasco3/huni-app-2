-- Deploy huni-backend:collection-record-user-column to pg
-- requires: collection-record-table

BEGIN;

ALTER TABLE huni.collection_record
 ADD COLUMN user_id INTEGER REFERENCES huni.user(id)
                    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;

UPDATE huni.collection_record cr
   SET user_id = c.user_id
  FROM (
      SELECT id, user_id
        FROM huni.collection
  ) c
 WHERE c.id = cr.collection_id;

ALTER TABLE huni.collection_record
    ALTER COLUMN user_id SET NOT NULL;

COMMIT;
