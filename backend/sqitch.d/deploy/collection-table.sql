-- Deploy collection-table
-- requires: user-table

BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE huni.collection (
    id              SERIAL PRIMARY KEY,

    user_id         INTEGER NOT NULL REFERENCES huni.user(id)
                    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE,

    name            text NOT NULL
                    CONSTRAINT nonempty_name CHECK (length(btrim(name)) > 0),
    synopsis        text,
    is_public       bool DEFAULT FALSE,
    created_utc     timestamp NOT NULL DEFAULT (now() at time zone 'utc'),
    modified_utc    timestamp NOT NULL DEFAULT (now() at time zone 'utc'),

    UNIQUE (user_id, name)
);

COMMIT;
