-- Deploy link-table-unique-function
-- requires: link-table

BEGIN;

CREATE OR REPLACE FUNCTION huni.check_linkpair_unique()
RETURNS TRIGGER AS $$
BEGIN
    IF EXISTS (
        SELECT me.id FROM huni.linkall me
        WHERE NEW.user_id     = me.user_id
          AND NEW.from_docid  = me.from_docid
          AND NEW.to_docid    = me.to_docid
          AND NEW.linktype_id = me.linktype_id
    ) THEN
        RAISE EXCEPTION 'Duplicate link exists';
    END IF;
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER check_linkpair_unique
    BEFORE INSERT OR UPDATE ON huni.link
    FOR EACH ROW EXECUTE PROCEDURE huni.check_linkpair_unique();

END;
