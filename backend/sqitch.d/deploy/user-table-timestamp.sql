-- Deploy user-table-timestamp
-- requires: user-table
-- requires: update-timestamp-function

BEGIN;

CREATE TRIGGER update_timestamp BEFORE UPDATE
    ON huni.user FOR EACH ROW EXECUTE PROCEDURE
    huni.update_timestamp();

COMMIT;
