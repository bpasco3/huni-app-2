-- Deploy linktype-table-timestamp
-- requires: linktype-table
-- requires: update-timestamp-function

BEGIN;

CREATE TRIGGER update_timestamp BEFORE UPDATE
    ON huni.linktype FOR EACH ROW EXECUTE PROCEDURE
    huni.update_timestamp();

COMMIT;
