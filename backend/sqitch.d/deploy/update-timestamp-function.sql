-- Deploy update-timestamp-function
-- requires: schema

BEGIN;

CREATE OR REPLACE FUNCTION huni.update_timestamp()
RETURNS TRIGGER AS $$
BEGIN
   NEW.modified_utc = now() at time zone 'utc';
   RETURN NEW;
END;
$$ language 'plpgsql';

COMMIT;
