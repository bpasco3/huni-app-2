-- Deploy huni-backend:collection-user-table to pg
-- requires: collection-role-table
-- requires: collection-table
-- requires: user-table

BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE huni.collection_user (
    collection_id   INTEGER NOT NULL REFERENCES huni.collection(id)
                    ON UPDATE CASCADE ON DELETE CASCADE,

    user_id         INTEGER NOT NULL REFERENCES huni.user(id)
                    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE,

    role_id         TEXT NOT NULL REFERENCES huni.collection_role(id)
                    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE,

    -- Who a user got invited by. The original user invites themself.
    inviting_user_id    INTEGER NOT NULL REFERENCES huni.user(id)
                    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE,

    PRIMARY KEY (collection_id, user_id, role_id),

    -- A user can only have one role in a collection
    UNIQUE (collection_id, user_id)
);

INSERT INTO huni.collection_user (collection_id, user_id, role_id, inviting_user_id)
     SELECT c.id, c.user_id, 'owner', c.user_id
       FROM huni.collection c;

COMMIT;
