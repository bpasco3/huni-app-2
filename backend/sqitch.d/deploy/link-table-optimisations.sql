-- Deploy link-table-optimisations
-- requires: link-table-unique-function

BEGIN;

-- The existing check_linkpair_unique function is very slow. Inserting 450k
-- links took nine hours. The slowness seems to have come from using the
-- linkall view to check for duplicates.

-- Instead we replace this with:
-- 1. a regular unique constraint for the forwards case
-- 2. a check against the link table for the reverse link

-- Add a regular unique constraint
ALTER TABLE huni.link ADD CONSTRAINT forwards_unique
    UNIQUE(user_id, from_docid, to_docid, linktype_id);


-- Add a new function to check reverse links.
CREATE OR REPLACE FUNCTION huni.check_reverse_link_unique()
RETURNS TRIGGER AS $$
BEGIN
    IF EXISTS (
        SELECT me.id FROM huni.link me
            JOIN huni.linktype lt ON me.linktype_id = lt.id
        WHERE NEW.user_id     = me.user_id
          AND NEW.from_docid  = me.to_docid
          AND NEW.to_docid    = me.from_docid
          AND NEW.linktype_id = lt.pair_id
    ) THEN
        RAISE EXCEPTION 'Duplicate link exists';
    END IF;
    RETURN NEW;
END;
$$ language 'plpgsql';

-- Remove the old trigger.
DROP TRIGGER IF EXISTS check_linkpair_unique ON huni.link;

-- Add the new trigger.
CREATE TRIGGER check_reverse_link_unique
    BEFORE INSERT OR UPDATE ON huni.link
    FOR EACH ROW EXECUTE PROCEDURE huni.check_reverse_link_unique();

COMMIT;
