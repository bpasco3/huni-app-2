-- Deploy aaf-jti-table
-- requires: schema

BEGIN;

CREATE TABLE huni.aaf_jti (
    jti         text    NOT NULL UNIQUE,
    expiry      integer NOT NULL
);

COMMIT;
