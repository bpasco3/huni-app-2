-- Deploy huni-backend:drop-collection-user-column to pg
-- requires: collection-user-table

BEGIN;

ALTER TABLE huni.collection
DROP COLUMN user_id;

COMMIT;
