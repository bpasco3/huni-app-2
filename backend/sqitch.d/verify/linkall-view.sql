-- Verify linkall-view

BEGIN;

-- XXX Add verifications here.
SELECT
    id,
    user_id,
    synopsis,
    from_docid,
    to_docid,
    linktype_id,
    created_utc,
    modified_utc
  FROM huni.linkall
  WHERE false;

ROLLBACK;
