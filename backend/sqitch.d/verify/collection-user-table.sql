-- Verify huni-backend:collection-user-table on pg

BEGIN;

SELECT collection_id,
       user_id,
       role_id,
       inviting_user_id
  FROM huni.collection_user
 WHERE false;

ROLLBACK;
