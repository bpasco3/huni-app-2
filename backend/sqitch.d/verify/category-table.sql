-- Verify category-table

BEGIN;

SELECT
    id,
    name
  FROM huni.category
  WHERE FALSE;

ROLLBACK;
