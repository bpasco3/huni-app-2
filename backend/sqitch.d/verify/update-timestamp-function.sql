-- Verify update-timestamp-function

BEGIN;

SELECT has_function_privilege('huni.update_timestamp()', 'execute');

ROLLBACK;
