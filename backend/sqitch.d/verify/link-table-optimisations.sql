-- Verify link-table-optimisations

BEGIN;

SELECT has_function_privilege(
    'huni.check_reverse_link_unique()', 'execute');

ROLLBACK;
