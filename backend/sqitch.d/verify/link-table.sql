-- Verify link-table

BEGIN;

SELECT
    id,
    user_id,
    synopsis,
    from_docid,
    to_docid,
    linktype_id,
    created_utc,
    modified_utc
  FROM huni.link
  WHERE FALSE;

ROLLBACK;
