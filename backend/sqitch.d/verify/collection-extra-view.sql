-- Verify huni-backend:collection-extra-view on pg

BEGIN;

SELECT
    collection_id,
    record_count,
    owners,
    editors,
    viewers
  FROM huni.collection_extra
  WHERE FALSE;

ROLLBACK;
