-- Verify user-table

BEGIN;

SELECT
    id,
    name,
    email,
    institutions,
    description,
    interests,
    collaboration_interests,
    auth_userid,
    auth_domain,
    created_utc,
    modified_utc
  FROM huni.user
  WHERE FALSE;

SELECT has_function_privilege('huni.system_user_id()', 'execute');

ROLLBACK;
