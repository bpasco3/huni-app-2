-- Verify schema

BEGIN;

SELECT pg_catalog.has_schema_privilege('huni', 'usage');

ROLLBACK;
