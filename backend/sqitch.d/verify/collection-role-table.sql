-- Verify huni-backend:collection-role-table on pg

BEGIN;

SELECT id
  FROM huni.collection_role
 WHERE false;

ROLLBACK;
