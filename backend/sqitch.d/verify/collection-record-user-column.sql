-- Verify huni-backend:collection-record-user-column on pg

BEGIN;

SELECT user_id
  FROM huni.collection_record
 WHERE false;

ROLLBACK;
