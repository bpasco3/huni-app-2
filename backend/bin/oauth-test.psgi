use Plack::Builder;
use Plack::App::Auth::OAuth2;

builder {
    enable 'Session::Cookie';
    Plack::App::Auth::OAuth2->new(providers => 'oauth.yml')->to_app;
};
