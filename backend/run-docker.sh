#!/bin/bash

set -e

sqitch deploy -t $HUNI_SQITCH_TARGET
PERL5LIB=./lib:$PERL5LIB ./tools/load-cms --dir cms --dsn "$HUNI_BACKEND_DSN"
PERL5LIB=./lib:$PERL5LIB ./tools/update-neo4j --schema neo4j-schema.yaml deploy
exec plackup --no-default-middleware -p 80 -I lib -s Starlet app.psgi
