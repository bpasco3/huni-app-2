#!/bin/bash -x
# test-suites, you!

if [ -n "$HUDSON_HOME" ]; then
    # Grab the submodules if we are running on hudson, otherwise assume the
    git submodule update --init --recursive
fi

export PERL5LIB=$PWD/lib:$PWD/t/lib:$PERL5LIB

# Enable parallel testing. Comment this out if it becomes problematic.
PARALLEL_JOBS=4

if [ -n $PARALLEL_JOBS ]; then
    export HARNESS_OPTIONS=j$PARALLEL_JOBS
    export PROVE_OPTIONS=-j$PARALLEL_JOBS
fi

# Our temporary directories don't seem to be cleanin up after themselves, so
# instead just put them in one place, and delete them all at the end.
# This relies on the temporary directories honouring $TMPDIR, but seems to work
# at the moment.
#export TMPDIR=$( mktemp -d )

perl Makefile.PL
make
rm -f junit_output.xml
./tools/start-test-db prove $PROVE_OPTIONS --blib --harness TAP::Harness::JUnit -r t/

cover -delete
HARNESS_PERL_SWITCHES=-MDevel::Cover=+ignore,.,-select,blib/lib/ ./tools/start-test-db make test 2>&1 | grep -v '(unknown origin)'
cover -outputdir cover_html

make distclean
rm -rf $TMPDIR
