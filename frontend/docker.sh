#!/bin/bash

set -e

CONTAINER_NAME=huni-frontend-running
IMAGE_NAME=huniteam/huni-frontend

if [ -n $2 ]; then
    TAG_NAME=$2
    IMAGE_NAME=$IMAGE_NAME:$TAG_NAME
fi

CONTAINER_PORT=80
LOCAL_PORT=3001

case $1 in

    build)
        make docker
        docker build -t $IMAGE_NAME .
        ;;

    push)
        docker push $IMAGE_NAME
        if [ -n $TAG_NAME ]; then
            git tag -f docker-image/$TAG_NAME
        fi
        ;;

    start|run)
        docker run -d --name $CONTAINER_NAME \
                -p $LOCAL_PORT:$CONTAINER_PORT \
                $IMAGE_NAME
        ;;

    stop)
        docker stop $CONTAINER_NAME
        docker rm $CONTAINER_NAME
        ;;

    restart)
        $0 stop
        $0 start
        ;;

    *)
        echo "usage: $0 [build|start|stop|restart|push]"
        exit 1
        ;;

esac

