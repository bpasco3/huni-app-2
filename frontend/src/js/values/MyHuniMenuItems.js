angular.module('huni').value('MyHuniMenuItems', [
    { page: 'collections',  text: 'My Collections' },
    { page: 'links',        text: 'My Links' },
    { page: 'profile',      text: 'Profile' }
]);
