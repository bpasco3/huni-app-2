angular.module('huni').factory('MakeDisplayName',
function() {

    function makeDisplayName(input) {
        if (input.displayName) {
            return input.displayName;
        }

        if (input.isDeleted) {
            return '[Deleted Record]';
        }

        // This record will have one of these:
        // * name only
        // * individualName and/or familyName
        // * none of the above
        // There's no overlap.
        return _.chain(['name', 'individualName', 'familyName'])
                .map(field => input[field])
                .compact()
                .value()
                .join(' ') || '[untitled]';
    }


    // There's only one function in this factory. Just return it.
    return makeDisplayName;
});
