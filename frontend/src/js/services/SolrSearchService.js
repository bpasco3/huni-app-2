angular.module('huni').factory('SolrSearchService', [
'$http', '$log', '$q', 'cfg', 'MakeDisplayName',
function($http, $log, $q, cfg, makeDisplayName) {


    function addDisplayNames(records) {
        _.each(records, record => {
            record.displayName = makeDisplayName(record);
        });
    }

    function doQuery(query) {
        return $http({
            method: 'POST',
            url: cfg.solr + '/select',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: query + '&wt=json',
        });
    }

    // We can get lots of recordIds here. Break them into chunks and do one
    // call on each chunk.
    function getRecords(recordIds) {
        const chunkSize = 1000;
        let queryPromises = [ ];
        while (recordIds.length > 0) {
            let chunk = recordIds.splice(0, chunkSize);
            queryPromises.push(getRecordsChunk(chunk));
        }

        // Wait for all queries to complete, then flatten the list of lists
        // into a single list.
        return $q.all(queryPromises).then(recordLists =>
            _.flatten(recordLists, true)
        );
    };

    // This looks up a single chunk of records.
    function getRecordsChunk(recordIds) {
        let query = 'rows=' + recordIds.length + '&q=' +
            _.chain(recordIds)
             .map(function(id) {
                // The stars in the id need to be escaped, ftw!
                return "docid:" + id.replace(/\*/g, '\\*'); })
             .join(' OR ')
             .value();
        return doQuery(query).then(function(resp) {
            let records = resp.data.response.docs;
            addDisplayNames(records);
            return records;
        });
    }

    /*
     * Given a list of partial records containing docid fields, lookup those
     * docids in solr, and merge the solr records with the partial records
     */
    function augmentRecords(dbRecords) {
        return getRecords(_.pluck(dbRecords, 'docid')).then(solrRecords => {
            addDisplayNames(solrRecords);
            let solrRecordMap = _.indexBy(solrRecords, 'docid');
            _.each(dbRecords, dbRecord => {
                let solrRecord = solrRecordMap[dbRecord.docid];
                _.extend(dbRecord, solrRecord);
            });
            return dbRecords;
        });
    };

    return {
        augmentRecords,
        doQuery,
        getRecords,
    };
}]);
