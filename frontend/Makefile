SRCDIR=src/js
TARGETDIR=app

TARGETSRCFILE=huni-src.js
TARGETLIBFILE=huni-lib.js
TARGETWGTFILE=huni-widget.js
TARGETWGT_pattern=huni-widget-%.js

TARGETSRC=$(TARGETDIR)/$(TARGETSRCFILE)
TARGETLIB=$(TARGETDIR)/$(TARGETLIBFILE)
TARGETWGT=$(TARGETDIR)/$(TARGETWGTFILE)
TARGETWGTS=$(TARGETDIR)/$(notdir $(wildcard $(SRCDIR)/huni-widget-*.js))
TARGETS=$(TARGETSRC) $(TARGETLIB) $(TARGETWGT) $(TARGETWGTS)

DESTROOT=/var/www/
CONFIG=default
CONFIG_FILE=src/config/$(CONFIG).js
WGT_CONFIG_FILE=src/config/wgt-$(CONFIG).js

ZIPEXTS=html css svg js htc map
DROP_CONSOLE=false

ANGULAR=angular-1.4.3

LIBS=\
	jquery-1.11.3.js \
	bootstrap-3.3.5/js/bootstrap.js \
	$(ANGULAR)/angular.js \
	$(ANGULAR)/angular-animate.js \
	$(ANGULAR)/angular-cookies.js \
	$(ANGULAR)/angular-route.js \
	angulartics-0.15.19/angulartics.js \
	angulartics-0.15.19/angulartics-ga.js \
	angular-shims-placeholder-0.2.0/angular-shims-placeholder.js \
	angular-summernote-0.4.0/src/angular-summernote.js \
	d3-3.5.6/d3.js \
	moment-2.10.6/moment.js \
	summernote-0.6.16/summernote.js \
	summernote-0.6.16/plugin/summernote-ext-video.js \
	ui-bootstrap-tpls-0.13.3.js \
	underscore-1.8.3/underscore.js \
	spin.min.js
LIBDIR=vendor-lib
LIBDEPS=$(addprefix $(LIBDIR)/,$(LIBS))


SOURCES=app.js controllers/*.js directives/*.js filters/*.js services/*.js \
		values/*.js
SRCDEPS=$(addprefix $(SRCDIR)/,$(SOURCES)) $(CONFIG_FILE)

WGT=huni-widget.js
WGTDEPS=$(WGT_CONFIG_FILE) $(addprefix $(SRCDIR)/,$(WGT))

all: $(TARGETS)

$(TARGETSRC): $(SRCDEPS)
	uglifyjs $(SRCDEPS) --output $(TARGETSRC) \
		--source-map $(TARGETSRC).map --source-map-url $(TARGETSRCFILE).map \
		--source-map-include-sources --comments --mangle \
		--compress warnings=false,drop_console=$(DROP_CONSOLE) \
		--preamble "// `git rev-parse HEAD` `date '+%x %X %Z'`"

$(TARGETWGT): $(WGTDEPS)
	uglifyjs $(WGTDEPS) --output $(TARGETWGT) \
		--source-map $(TARGETWGT).map --source-map-url $(TARGETWGTFILE).map \
		--source-map-include-sources --comments --mangle \
		--compress warnings=false,drop_console=$(DROP_CONSOLE) \
		--preamble "// `git rev-parse HEAD` `date '+%x %X %Z'`"

$(TARGETDIR)/$(TARGETWGT_pattern): $(SRCDIR)/$(TARGETWGT_pattern)
	uglifyjs $< --output $@ \
		--source-map $@.map --source-map-url $(notdir $@.map) \
		--source-map-include-sources --comments --mangle \
		--compress warnings=false,drop_console=$(DROP_CONSOLE) \
		--preamble "// `git rev-parse HEAD` `date '+%x %X %Z'`"

$(TARGETLIB): $(LIBDEPS)
	uglifyjs $(LIBDEPS) --output $(TARGETLIB) \
		--source-map $(TARGETLIB).map --source-map-url $(TARGETLIBFILE).map \
		--source-map-include-sources --comments --mangle \
		--preamble "// `git rev-parse HEAD` `date '+%x %X %Z'`"

package: $(TARGETS)
	debuild -uc -us -b

auto:
	./scripts/auto-rebuild

clean:
	find app -type f -name '*.gz' -delete
	rm -f $(TARGETS) $(addsuffix .map,$(TARGETS))

lint:
	jslint $(SRCDEPS)

zip: $(TARGETS)
	./scripts/find-ext app $(ZIPEXTS) | xargs ./scripts/gzip-keep

install: $(TARGETS)
	sudo rsync -av -delete app/ $(DESTROOT)
	git tag -f installed

dev01:
	$(MAKE) -B all CONFIG=dev01 DROP_CONSOLE=true

docker:
	$(MAKE) -B clean zip CONFIG=docker DROP_CONSOLE=true

staging:
	$(MAKE) -B clean zip CONFIG=staging DROP_CONSOLE=true
